﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using WPM;

[RequireComponent(typeof(VideoPlayer))]
public class VideoPlayerSkybox : MonoBehaviour {

    public Material skyboxMaterial;
    public Material originalSkyboxMaterial;
    private VideoPlayer videoPlayer;

    void Awake() {
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.prepareCompleted += VideoPlayerOnPrepareCompleted;
    }

    private void VideoPlayerOnPrepareCompleted(VideoPlayer source) {
        RenderSettings.skybox = skyboxMaterial;
    }

    private void OnDestroy() {
        videoPlayer.prepareCompleted -= VideoPlayerOnPrepareCompleted;
        RenderSettings.skybox = originalSkyboxMaterial;
    }
}
