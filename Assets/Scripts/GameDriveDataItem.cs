﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts {

    public class GameDriveData {
        public List<GameDrive> GameDrives { get; set; }
    }

    public class GameDrive {
        public string Name { get; set; }
        public List<GameDriveDataItem> Items { get; set; }
    }

    public class GameDriveDataItem {
        public GameDriveEventType EventType { get; set; }
        public string BundleGroup { get; set; }
        public string TrackingID { get; set; }
        public string MediaID { get; set; }
        public string MediaName { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string ResolutionBase { get; set; }
        public float StartPositionX { get; set; }
        public float StartPositionY { get; set; }
        public float StartPositionZ { get; set; }
        public float EndPositionX { get; set; }
        public float EndPositionY { get; set; }
        public float EndPositionZ { get; set; }
        public GameDriveShape Shape { get; set; }
        public int BoundsHeight { get; set; }
        public int BoundsWidth { get; set; }
        public bool IsPause { get; set; }
        public bool IsEnd { get; set; }
        public float SpeedModifier { get; set; }
        public float SpeedModifierDuration { get; set; }
        public string SelectAction1 { get; set; }
        public string SelectAction2 { get; set; }
        public string SelectAction3 { get; set; }
        public string QuestionContent { get; set; }
        public string QuestionAnswer1 { get; set; }
        public string QuestionAnswer2 { get; set; }
        public string QuestionAnswer3 { get; set; }
        public string QuestionAnswer4 { get; set; }
        public string QuestionCorrectAnswer { get; set; }
        public string LinkToSource { get; set; }
        public string QuestionCorrectAnswerAction { get; set; }
        public string QuestionIncorrectAnswerAction { get; set; }
        public float QuestionCorrectAnswerScore { get; set; }
        public float QuestionIncorrectAnswerScore { get; set; }
        public string InformationContent { get; set; }
        public string InformationContentTextLine1 { get; set; }
        public string InformationContentTextLine2 { get; set; }
        public string InformationContentTextLine3 { get; set; }
        public string InformationContentTextLine4 { get; set; }
        public string InformationContentTextLine5 { get; set; }

        public static GameDriveEventType GetEventType(string eventType) {
            if (eventType.ToLowerInvariant().Equals("motion tracking")) {
                return GameDriveEventType.MotionTracking;
            } else if (eventType.ToLowerInvariant().Equals("question selection")) {
                return GameDriveEventType.QuestionSelection;
            } else if (eventType.ToLowerInvariant().Equals("information box")) {
                return GameDriveEventType.InformationBox;
            } else {
                return GameDriveEventType.Unknown;
            }
        }

        public static GameDriveShape GetShape(string shape) {
            if (shape.ToLowerInvariant().Equals("ellipse")) {
                return GameDriveShape.Ellipse;
            } else if (shape.ToLowerInvariant().Equals("rectangle")) {
                return GameDriveShape.Rectangle;
            } else {
                return GameDriveShape.Unknown;
            }
        }

        public static TimeSpan ParseTime(string time) {
            string[] parts = time.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 4) {
                return TimeSpan.MinValue;
            }
            int ms = (int)(float.Parse(parts[3]) / 30f);
            return new TimeSpan(0, int.Parse(parts[0]), int.Parse(parts[1]), int.Parse(parts[2]), int.Parse(parts[3]));
        }
    }

    public enum GameDriveEventType {
        MotionTracking, QuestionSelection, InformationBox, Unknown
    }

    public enum GameDriveShape {
        Ellipse, Rectangle, Unknown
    }
}
