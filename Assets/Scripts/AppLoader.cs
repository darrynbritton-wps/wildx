﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using Assets.Scripts;
using Assets.Scripts.Utils;

using Azure.StorageServices;

using ControllerSelection;

using CSVFile;
using Microsoft.AppCenter.Unity.Analytics;
using Microsoft.AppCenter.Unity.Crashes;
using Newtonsoft.Json;

using TMPro;
using UnityEngine;
using UnityEngine.UI;

using VRStandardAssets.Utils;

using WPM;

using static Main;

public class AppLoader : MonoBehaviour {

    public static AppLoader Instance;
    private static bool Testing = false;
    //private static bool UseCache = false;
    private static bool ForceCache = false;
    //private static int CacheDuration = 120;
    private static string APIUrl = "http://40.71.210.65:8096";
    private static string AZBaseUrl = "https://management.azure.com";
    private static string AZUrl = "https://management.azure.com/subscriptions/e877a478-df73-491c-9c9c-947a9f80fcc3/resourceGroups/wpsVRApp/providers/Microsoft.Media/mediaServices/wpsvrmedia/{0}?api-version=2018-07-01";
    public static string ProxyUrl = "https://wildx.azurewebsites.net";
    private static string ResourceGroupName = "wpsVRApp";
    private static string MediaAccountName = "wpsvrmedia";
    private static string StorageAccountName = "wpsvr";
    private bool isOfflineMode = false;

    [SerializeField] public GameObject m_UI;
    [SerializeField] public GameObject m_VideoCanvas;
    [SerializeField] public GameObject m_Loading;
    [SerializeField] public GridLayoutGroup m_Grid;
    [SerializeField] public GameObject m_Template;
    [SerializeField] public GameObject m_EmptyText;
    [SerializeField] public GameObject m_World;
    [SerializeField] public WorldMapGlobe m_Globe;
    [SerializeField] public GameObject m_AllMarker;
    [SerializeField] public GameObject m_SpecialMarker;
    [SerializeField] public GameObject m_BaseMarker;
    [SerializeField] public GameObject m_LeftArrow;
    [SerializeField] public GameObject m_RightArrow;
    [SerializeField] public GameObject m_FilterPanel;
    [SerializeField] public GameObject m_FilterButton;
    [SerializeField] public GameObject m_Title;
    [SerializeField] public ScrollRect m_ScrollPanel;
    [SerializeField] public GameObject m_Splash;
    [SerializeField] public GameObject m_MainMenu;

    public string AccessToken;
    public VideoData Data;
    public GameDriveData GDData;
    public List<Marker> Markers;
    public string CurrentRegion;
    public string FilterRegion;
    public List<VideoDataItem> CurrentVideos;
    public Dictionary<string, bool?> FilterSpeciesCategory;
    public Dictionary<string, bool?> FilterPrimarySpecies, CurrentPrimarySpecies;
    public Dictionary<string, bool?> FilterIUCN;
    public Dictionary<string, bool?> FilterPopulationTrend;

    public StorageServiceClient StorageClient;
    public BlobService BlobService;

    private bool usingCDNCache = false;
    private bool usingGameDriveCache = false;
    private string tutorialText;
    private bool startupTutorialShown = false;

    private bool ShowGraphicDeath = false, ShowGraphicSex = false;

    public enum SortOrder {
        MostViewed, TopRated, Newest, Favorites, Downloads
    }
    public SortOrder Order = SortOrder.MostViewed;

    async void Start() {
        Instance = this;
        Trace.Log("VideoLoader Startup");
        //UseCache = Trace.IsDebug;
        HideMainMenu();
        m_Splash.SetActive(true);
        if (Trace.UseOculusInput) {
            InitOculus();
        }
        StartCoroutine(WaitForSplash());
        Trace.Log("Start loading videos");
        await LoadAsync();
    }

    public void Update() {
        OVRInput.Update();
    }

    void OnDestroy() {

    }

    private void InitOculus() {
        try {
            //GameObject.Find("OVRCameraRig").GetComponent<VREyeRaycaster>().enabled = false;
            //GameObject.Find("OVRCameraRig").GetComponent<MouseLook>().enabled = false;
            //GameObject.Find("OVRCameraRig").GetComponent<OVRRawRaycaster>().enabled = true;
            //GameObject.Find("OVRCameraRig").GetComponent<OVRPointerVisualizer>().enabled = true;
            Oculus.Platform.Core.AsyncInitialize();
            Oculus.Platform.Entitlements.IsUserEntitledToApplication().OnComplete(EntitlementCallback);
        } catch (Exception e) {
            Debug.LogError("Platform failed to initialize due to exception: " + e.ToString());
            Debug.LogException(e);
            Application.Quit();
        }
    }

    private void EntitlementCallback(Oculus.Platform.Message msg) {
        if (msg.IsError) {
            Application.Quit();
        } else {
            Trace.Log("You are entitled to use this app.");
        }
    }

    private IEnumerator WaitForSplash() {
        yield return new WaitForSeconds(5f);
        m_Splash.SetActive(false);
        //if (!startupTutorialShown) {
        //    ShowTutorial();
        //}
        ShowMainMenu();
        //ShowModeEncounters();
    }

    private void ShowMainMenu() {
        m_MainMenu.SetActive(true);
    }

    private void HideMainMenu() {
        m_MainMenu.SetActive(false);
    }

    public void ShowModeEncounters() {
        HideMainMenu();
        LoadMarkerData();
        ShowTutorial();
    }

    public void ShowModeSafaris() {
        Trace.Log("Showing Safaris");
        HideMainMenu();
        m_World.SetActive(false);
        VideoDataItem video = new VideoDataItem();
        video.ID = "7d307028-9ddf-461f-8938-3aef048c6ce7";
        video.Name = "Test Safari Video";
        video.Overview = "Test Safari Video";
        video.Thumbnail = string.Format("asset-{0}/poster.png", video.ID);
        video.Metadata = new List<VideoAttribute>();
        var path = string.Format("{0}/downloads/{1}", Application.persistentDataPath, video.ID);
        video.IsDownloaded = File.Exists(path);
        Events.Instance.PlayTestSafariVideo(GDData.GameDrives.FirstOrDefault(_ => _.Name.Equals("9531b8bc-c7f3-4741-a050-d984d430d388")), video);
    }

    public void ShowModeExplorer() {
        HideMainMenu();
    }

    public void CheckConnectivity() {
        isOfflineMode = Application.internetReachability == NetworkReachability.NotReachable;
        Trace.Log("Internet Connection: " + Application.internetReachability.ToString() + " (Offline: " + isOfflineMode + ")");
    }

    async Task LoadAsync() {
        Trace.Log("Connectivity: " + Application.internetReachability.ToString());
        CheckConnectivity();
        if (isOfflineMode) {
            Trace.Log("No internet connection, using cache");
            Trace.UseMetadataCache = true;
            Trace.UseYoutubeCache = true;
        }
        if (m_AllMarker) {
            m_AllMarker.SetActive(false);
        }
        if (m_SpecialMarker) {
            m_SpecialMarker.SetActive(false);
        }
        m_UI.SetActive(true);
        m_VideoCanvas.SetActive(false);
        m_Loading.SetActive(true);
        await GetVideoDataAsync();
        //if (m_AllMarker) {
        //    m_AllMarker.SetActive(true);
        //}
        //if (m_SpecialMarker) {
        //    m_SpecialMarker.SetActive(true);
        //}
    }

    private void UpdateFavorites() {
        string favoritesValue = Preferences.GetString("Favorites", "");
        if (!string.IsNullOrEmpty(favoritesValue)) {
            VideoFavorites favorites = null;
            try {
                favorites = JsonConvert.DeserializeObject<VideoFavorites>(favoritesValue);
            } catch (Exception e) {
                Trace.LogError(e.ToString());
            }
            if (favorites != null && favorites.Favorites != null) {
                foreach (var favorite in favorites.Favorites) {
                    var video = Data.Videos.FirstOrDefault(_ => _.ID == favorite.ID);
                    if (video != null) {
                        video.IsFavorite = favorite.IsFavorite;
                    }
                }
            }
        }
    }

    public void UpdateDownloads() {
        foreach (var video in Data.Videos) {
            var path = string.Format("{0}/downloads/{1}", Application.persistentDataPath, video.ID);
            video.IsDownloaded = File.Exists(path);
        }
    }

    private void LoadMarkerData() {
        if (Data == null) {
            m_Loading.FindObject("Text").GetComponent<TMP_Text>().text = "Failed to load data";
            return;
        }
        UpdateFavorites();
        UpdateDownloads();
        Markers = GetMarkers();

        if (Markers != null) {
            //Trace.Log("Markers: " + Markers.Count);
            foreach (var marker in Markers) {
                ////Trace.Log("Add marker: " + marker.Title);
                var markerObject = Instantiate(m_BaseMarker);
                markerObject.name = marker.Title;
                m_Globe.calc.fromUnit = UNIT_TYPE.DecimalDegrees;
                m_Globe.calc.fromLatDec = marker.Latitude;
                m_Globe.calc.fromLonDec = marker.Longitude;
                if (m_Globe.calc.Convert()) {
                    var position = m_Globe.calc.toSphereLocation;
                    //Trace.Log("Position: " + position);
                    //markerObject.GetComponent<Marker>().Title = marker.Title;
                    var mapItem = markerObject.GetComponent<InteractiveMapItem>();
                    mapItem.m_Region.text = marker.Title;
                    markerObject.SetActive(true);
                    m_Globe.AddMarker(markerObject, position, 0.1f);
                }
            }
        }
        //StartCoroutine(LoadThumbnailCache());
        m_Loading.SetActive(false);
        m_UI.SetActive(false);
        m_Splash.SetActive(false);
    }

    public async Task<string> GetVideoUrl(VideoDataItem video) {
        Trace.Log(JsonConvert.SerializeObject(video));
        AzureStreamingLocators locators = await Web.Post<AzureStreamingLocators>(string.Format(AZUrl, string.Format("assets/{0}/listStreamingLocators", video.ID)), AccessToken);
        Trace.Log(JsonConvert.SerializeObject(locators));
        if (locators != null && locators.StreamingLocators != null && locators.StreamingLocators.Count > 0) {
            var locator = locators.StreamingLocators.FirstOrDefault();
            AzureStreamingPaths paths = await Web.Post<AzureStreamingPaths>(string.Format(AZUrl, string.Format("streamingLocators/{0}/listPaths", locator.StreamingLocatorID)), AccessToken);
            AzureStream stream = null;
            if (paths != null && paths.Streams != null && paths.Streams.Count > 0) {
                Trace.Log(JsonConvert.SerializeObject(paths));
                stream = paths.Streams.FirstOrDefault(_ => _.Protocol.Equals("Dash"));
            }
            if (stream != null && stream.Paths != null && stream.Paths.Count > 0) {
                //return string.Format("https://wpsvrmedia-usea.streaming.media.azure.net{0}?api-version=2018-07-01", stream.Paths.FirstOrDefault());
                //return string.Format("https://premium-wpsvrmedia-usea.streaming.media.azure.net{0}?api-version=2018-07-01", stream.Paths.FirstOrDefault());
                string path = stream.Paths.Where(_ => _.Contains("csf")).FirstOrDefault();
                if (path != null) {
                    path = path.Replace("csf", "csf,filter=HQ");
                    return string.Format("https://premium-wpsvrmedia-usea.streaming.media.azure.net{0}?api-version=2018-07-01", path);
                }
            }
        }
        return "";
    }

    public async Task<string> GetVideoDownloadUrl(VideoDataItem video) {
        Trace.Log(JsonConvert.SerializeObject(video));
        string request = JsonConvert.SerializeObject(new { permissions = "Read", expiryTime = XmlConvert.ToString(DateTime.UtcNow.AddDays(1), XmlDateTimeSerializationMode.RoundtripKind) });
        AzureContainerURLs urls = await Web.Post<AzureContainerURLs>(string.Format(AZUrl, string.Format("assets/{0}/listContainerSas", video.ID)), request, AccessToken);
        Trace.Log(JsonConvert.SerializeObject(urls));
        if (urls != null && urls.URLs != null && urls.URLs.Count > 0) {
            return urls.URLs.FirstOrDefault();
        }
        return "";
    }

    private async Task GetVideoDataAsync() {

        // Check for local tutorial text
        var resource = Resources.Load<TextAsset>("tutorial");
        if (resource != null) {
            tutorialText = resource.text;
            //if (!string.IsNullOrEmpty(tutorialText)) {
            //    startupTutorialShown = true;
            //    ShowTutorial();
            //}
        }

        Data = new VideoData();
        Data.Videos = new List<VideoDataItem>();

        if (string.IsNullOrEmpty(AccessToken)) {
            Trace.Log("No access token - logging in");
            AccessToken = await Web.Get(string.Format("{0}/proxy/token", ProxyUrl), null);
            Trace.Log("Got access token: " + AccessToken);
            Analytics.TrackEvent(Events.EventAuthAzure, new Events.EventTraceProperties().Add(Events.GetProperties()).Properties);
        }
        if (string.IsNullOrEmpty(AccessToken)) {
            Trace.Log("Failed to retrieve auth token from Azure");
            //m_Loading.FindObject("Text").GetComponent<TMP_Text>().text = "Failed to load data";
            Crashes.TrackError(new Exception("Failed to retrieve auth token from Azure"), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
            Trace.UseMetadataCache = true;
            Trace.UseYoutubeCache = true;
            ForceCache = true;
            isOfflineMode = true;
        }

        StorageClient = new StorageServiceClient("https://wpsvr.blob.core.windows.net/", "6Enz3KLUSzJzMrDHGkFHUYnj+b6vb6BuONBzB5tgwQwHpRDo/Y/QHAopvkN6P4bUqF0shcfi7L/9oLS9PBHU+Q==");
        BlobService = StorageClient.GetBlobService();

        if (Trace.UseMetadataCache) {
            // Check cache
            Trace.Log("CDN Cache Date: " + Preferences.CDNCacheDate);
            var cacheAge = DateTime.Now.Subtract(Preferences.CDNCacheDate).TotalMinutes;
            Trace.Log("CDN Cache Age: " + cacheAge);
            if (cacheAge < Trace.CacheDuration || ForceCache) {
                if (!string.IsNullOrEmpty(Preferences.CDNData)) {
                    Trace.Log("Using cached CDN data");
                    Trace.Log(Preferences.CDNData);
                    Data = JsonConvert.DeserializeObject<VideoData>(Preferences.CDNData);
                    usingCDNCache = true;
                }
            }
        }
        if (Trace.UseGameDriveCache) {
            // Check cache
            Trace.Log("GameDrive Cache Date: " + Preferences.GameDriveCacheDate);
            var cacheAge = DateTime.Now.Subtract(Preferences.GameDriveCacheDate).TotalMinutes;
            Trace.Log("GameDrive Cache Age: " + cacheAge);
            if (cacheAge < Trace.CacheDuration || ForceCache) {
                if (!string.IsNullOrEmpty(Preferences.GameDriveData)) {
                    Trace.Log("Using cached game drive data");
                    Trace.Log(Preferences.GameDriveData);
                    GDData = JsonConvert.DeserializeObject<GameDriveData>(Preferences.GameDriveData);
                    usingGameDriveCache = true;
                }
            }
        }
        Trace.Log("Tutorial shown: " + Preferences.TutorialShown.ToString());
        if (!usingCDNCache) {
            await BlobService.ListContainers(async (results) => {
                Trace.Log(JsonConvert.SerializeObject(results));
                if (results != null && results.IsError) {
                    if (!usingCDNCache) {
                        Data = JsonConvert.DeserializeObject<VideoData>(Preferences.CDNData);
                        isOfflineMode = true;
                        if (Data == null || Data.Videos == null || Data.Videos.Count == 0) {
                            Events.Instance.ShowError(string.Format("Could not download data, please try close and relaunch the app.\n\n({0})", results.ErrorMessage));
                        }
                    }
                    m_Loading.SetActive(false);
                } else if (results != null && results.Data != null && results.Data.Containers != null) {
                    Trace.Log(results.Data.Containers.Length.ToString() + " container(s)");
                    foreach (var container in results.Data.Containers) {
                        // Universal metadata
                        if (container.Name.Equals("metadata")) {
                            ////Trace.Log("Container: " + container.Name);
                            await BlobService.ListBlobs(async (blobs) => {
                                if (blobs != null && blobs.Data != null && blobs.Data.Blobs != null) {
                                    //Trace.Log(container.Name + ": " + blobs.Data.Blobs.Length.ToString() + " blob(s)");
                                    foreach (var blob in blobs.Data.Blobs) {
                                        Trace.Log("Metadata blob: " + blob.Name);
                                        string path = string.Format("{0}/{1}", container.Name, blob.Name);
                                        if (blob.Name.Equals("tutorial.txt")) {
                                            await BlobService.GetTextBlob((data) => {
                                                if (data.IsError) {
                                                    Trace.LogError("Error: " + JsonConvert.SerializeObject(data));
                                                    return;
                                                }
                                                tutorialText = data.Content;
                                            }, path);
                                        } else if (blob.Name.Equals("metadata.csv")) {
                                            await BlobService.GetTextBlob((data) => {
                                                if (data.IsError) {
                                                    Trace.LogError("Error: " + JsonConvert.SerializeObject(data));
                                                    return;
                                                }
                                                Trace.Log("Got metadata: " + data.Content);
                                                using (StreamReader stream = new StreamReader(Main.Strings.Stream(data.Content))) {
                                                    using (CSVReader reader = new CSVReader(stream, new CSVSettings { AllowNull = true, HeaderRowIncluded = true, SkipLines = 1, FieldDelimiter = ',', TextQualifier = '\"' })) {
                                                        Trace.Log("Header: " + JsonConvert.SerializeObject(reader.Headers));
                                                        foreach (var line in reader.Lines()) {
                                                            VideoDataItem item = new VideoDataItem();
                                                            item.ID = line[0];
                                                            item.Name = line[1];
                                                            item.Overview = line[2];
                                                            item.Thumbnail = string.Format("asset-{0}/poster.png", item.ID);
                                                            item.Metadata = new List<VideoAttribute>();
                                                            for (int index = 3; index < reader.Headers.Length; index++) {
                                                                item.Metadata.Add(new VideoAttribute { Name = reader.Headers[index], Value = line[index] });
                                                            }
                                                            Data.Videos.Add(item);
                                                        }
                                                    }
                                                }
                                            }, path);
                                        } else if (blob.Name.ToLower().Equals("game_drive_metadata.csv")) {
                                            await BlobService.GetTextBlob((data) => {
                                                if (data.IsError) {
                                                    Trace.LogError("Error: " + JsonConvert.SerializeObject(data));
                                                    return;
                                                }
                                                Trace.Log("Got game drive metadata: " + data.Content);
                                                GDData = new GameDriveData();
                                                GDData.GameDrives = new List<GameDrive>();
                                                using (StreamReader stream = new StreamReader(Main.Strings.Stream(data.Content))) {
                                                    using (CSVReader reader = new CSVReader(stream, new CSVSettings { AllowNull = true, HeaderRowIncluded = true, SkipLines = 1, FieldDelimiter = ',', TextQualifier = '\"' })) {
                                                        Trace.Log("Header: " + JsonConvert.SerializeObject(reader.Headers));
                                                        int index = 1;
                                                        GameDrive gameDrive = null;
                                                        foreach (var line in reader.Lines()) {
                                                            if (gameDrive == null || !gameDrive.Name.Equals(line[3])) {
                                                                gameDrive = new GameDrive {
                                                                    Name = line[3]
                                                                };
                                                                gameDrive.Items = new List<GameDriveDataItem>();
                                                                GDData.GameDrives.Add(gameDrive);
                                                            }
                                                            GameDriveDataItem item = new GameDriveDataItem();
                                                            item.EventType = GameDriveDataItem.GetEventType(line[0]);
                                                            item.BundleGroup = line[1];
                                                            item.TrackingID = line[2];
                                                            item.MediaID = line[3];
                                                            item.MediaName = line[4];
                                                            if (!string.IsNullOrWhiteSpace(line[5]) && !string.IsNullOrWhiteSpace(line[6])) {
                                                                //Trace.Log("Times: " + line[5] + "; " + line[6]);
                                                                item.StartTime = GameDriveDataItem.ParseTime(line[5]);
                                                                item.EndTime = GameDriveDataItem.ParseTime(line[6]);
                                                            }
                                                            item.ResolutionBase = line[7];
                                                            if (!string.IsNullOrWhiteSpace(line[8]) && !string.IsNullOrWhiteSpace(line[9]) && !string.IsNullOrWhiteSpace(line[10]) &&
                                                                !string.IsNullOrWhiteSpace(line[11]) && !string.IsNullOrWhiteSpace(line[12]) && !string.IsNullOrWhiteSpace(line[13])) {
                                                                try {
                                                                    item.StartPositionX = float.Parse(line[8], CultureInfo.InvariantCulture);
                                                                    item.StartPositionY = float.Parse(line[9], CultureInfo.InvariantCulture);
                                                                    item.StartPositionZ = float.Parse(line[10], CultureInfo.InvariantCulture);
                                                                    item.EndPositionX = float.Parse(line[11], CultureInfo.InvariantCulture);
                                                                    item.EndPositionY = float.Parse(line[12], CultureInfo.InvariantCulture);
                                                                    item.EndPositionZ = float.Parse(line[13], CultureInfo.InvariantCulture);
                                                                } catch (Exception e) {
                                                                    Trace.LogError(string.Format("Invalid position data at line '{0}': {1}", index, e.Message));
                                                                }
                                                            }
                                                            item.Shape = GameDriveDataItem.GetShape(line[14]);
                                                            if (!string.IsNullOrWhiteSpace(line[15]) && !string.IsNullOrWhiteSpace(line[16])) {
                                                                try {
                                                                    item.BoundsHeight = int.Parse(line[15], CultureInfo.InvariantCulture);
                                                                    item.BoundsWidth = int.Parse(line[16], CultureInfo.InvariantCulture);
                                                                } catch (Exception e) {
                                                                    Trace.LogError(string.Format("Invalid bounds data at line '{0}': {1}", index, e.Message));
                                                                }
                                                            }
                                                            if (!string.IsNullOrWhiteSpace(line[17])) {
                                                                item.IsPause = bool.Parse(line[17]);
                                                            }
                                                            gameDrive.Items.Add(item);
                                                            index++;
                                                        }
                                                    }
                                                }
                                            }, path);
                                            Trace.Log(JsonConvert.SerializeObject(GDData));
                                            Preferences.GameDriveData = JsonConvert.SerializeObject(GDData);
                                            Preferences.GameDriveCacheDate = DateTime.Now;
                                        }
                                    }
                                    await LoadVideoData();
                                }
                            }, container.Name);
                        }
                    }
                }
            });
            Analytics.TrackEvent(Events.EventVideoListLoadAzure, new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
        } else {
            Analytics.TrackEvent(Events.EventVideoListLoadCache, new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
            await LoadVideoData();
        }
    }

    private async Task LoadVideoData() {
        try {
            var cacheAge = DateTime.Now.Subtract(Preferences.YouTubeCacheDate).TotalMinutes;
            Trace.Log("Youtube Cache Age: " + cacheAge);
            if (cacheAge > Trace.CacheDuration) {
                Trace.UseYoutubeCache = false;
            }
            if (!Trace.UseYoutubeCache) {
                Trace.Log("Youtube data cache stale: Updating data.");
                Data = await UpdateYouTubeVideoData(Data);
                Preferences.YouTubeCacheDate = DateTime.Now;
            } else {
                List<VideoDataItem> cacheData = null;
                try {
                    cacheData = JsonConvert.DeserializeObject<List<VideoDataItem>>(Preferences.YouTubeData);
                } catch { }
                UpdateVideoDataFromYoutube(Data, cacheData);
            }
            Preferences.CDNData = JsonConvert.SerializeObject(Data);
            Preferences.CDNCacheDate = DateTime.Now;
            await LoadThumbnailCache();
        } catch (Exception e) {
            Trace.LogError(e.ToString());
            Crashes.TrackError(new Exception(string.Format("Failed to augment YouTube data: {0}", e.Message)), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
        }
        UpdateFavorites();
    }

    private void ShowTutorial() {
        if (string.IsNullOrEmpty(tutorialText)) {
            // Get cached tutorial?
            tutorialText = Preferences.TutorialText;
        }
        if (!string.IsNullOrEmpty(tutorialText)) {
            Preferences.TutorialText = tutorialText;
            Events.Instance.TutorialText = tutorialText;
            if (!Preferences.TutorialShown) {
                Events.Instance.ShowTutorial();
            }
        }
    }

    public void SetFavorites() {
        VideoFavorites favorites = new VideoFavorites();
        foreach (var video in Data.Videos) {
            if (video.IsFavorite) {
                favorites.Favorites.Add(new VideoFavorite { ID = video.ID, IsFavorite = true });
            }
        }
        Preferences.SetString("Favorites", JsonConvert.SerializeObject(favorites));
    }

    private static async Task<bool> GetNfoBlobData(BlobService client, VideoDataItem item, Container container, Blob blob) {
        bool add = false;
        string path = string.Format("{0}/{1}", container.Name, blob.Name);
        var blobClient = await client.GetBlob((data) => {
            if (data.IsError) {
                //Trace.Log("Error: " + JsonConvert.SerializeObject(data));
            }
            if (data != null && data.Data != null) {
                ////Trace.Log("XML: " + Encoding.UTF8.GetString(data.Data));
                var xml = Encoding.UTF8.GetString(data.Data);
                string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                if (xml.StartsWith(_byteOrderMarkUtf8)) {
                    xml = xml.Remove(0, _byteOrderMarkUtf8.Length);
                }
                //Trace.Log(xml);
                XmlDocument document = new XmlDocument();
                document.LoadXml(xml);
                XmlNodeList nodes = document.GetElementsByTagName("title");
                if (nodes != null && nodes.Count > 0) {
                    item.Name = nodes[0].InnerText;
                }
                nodes = document.GetElementsByTagName("plot");
                if (nodes != null && nodes.Count > 0) {
                    item.Overview = nodes[0].InnerText;
                }
                nodes = document.GetElementsByTagName("tag");
                if (nodes != null && nodes.Count > 0) {
                    item.Metadata = new List<VideoAttribute>();
                    for (int index = 0; index < nodes.Count; index++) {
                        var node = nodes[index];
                        if (node.InnerText.IndexOf(":") > -1) {
                            string name = node.InnerText.Substring(0, node.InnerText.IndexOf(":"));
                            string value = node.InnerText.Substring(node.InnerText.IndexOf(":") + 1);
                            // Cleanup
                            name = name.Replace("  ", " ").Trim();
                            value = value.Replace("  ", " ").Trim();
                            //
                            item.Metadata.Add(new VideoAttribute { Name = name, Value = value });
                        }
                        /*string[] parts = node.InnerText.Split(new string[] { ":" }, 2, StringSplitOptions.None);
                        if (parts != null && parts.Length == 2) {
                            item.Metadata.Add(new VideoAttribute { Name = parts[0].Trim(), Value = parts[1].Trim() });
                        }*/
                    }
                }
                add = true;
            }
        }, path);
        return add;
    }

    #region Unused

    //private static async Task<ServiceClientCredentials> GetCredentialsAsync() {
    //    ClientCredential clientCredential = new ClientCredential("80cc3402-b07d-492f-a2f4-e91b1a90b9fb", "TNAq8h0m6.n7M.J~v6xgm-F8N7M-AEeu6J");
    //    return await ApplicationTokenProvider.LoginSilentAsync("97d7aef4-3768-471f-bfa2-fac54183d5a5", clientCredential, ActiveDirectoryServiceSettings.Azure);
    //}

    //private static async Task<IAzureMediaServicesClient> CreateMediaServicesClientAsync() {
    //    var credentials = await GetCredentialsAsync();
    //    return new AzureMediaServicesClient(new Uri("https://management.azure.com"), credentials) {
    //        SubscriptionId = "e877a478-df73-491c-9c9c-947a9f80fcc3",
    //    };
    //}

    //public async Task<string> GetVideoUrl2(VideoDataItem video) {
    //    //Trace.Log("User: " + GetApiClient().CurrentUserId);
    //    var result = await GetApiClient().PostPlaybackInfo(new PlaybackInfoRequest {
    //        AutoOpenLiveStream = true,
    //        MaxStreamingBitrate = 20000000,
    //        StartTimeTicks = 0,
    //        Id = video.ID,
    //        UserId = GetApiClient().CurrentUserId,
    //        MediaSourceId = video.ID
    //    });
    //    if (result.ErrorCode != null && result.ErrorCode.HasValue) {
    //        throw new Exception(result.ErrorCode.Value.ToString());
    //    }
    //    //Trace.Log(JsonConvert.SerializeObject(result));
    //    if (result.MediaSources != null && result.MediaSources.Length > 0) {
    //        //return string.Format("{0}{1}", APIUrl, string.Format("/videos/{0}/master.m3u8?DeviceId=X1&MediaSourceId={0}&VideoCodec=h264&AudioCodec=mp3,aac&VideoBitrate=119808000&AudioBitrate=192000&PlaySessionId={1}&api_key=c6c98582c31747dc95049c9ba2e5c732&TranscodingMaxAudioChannels=2&RequireAvc=false&Tag=40e66b1d9567d716cfffa9aba5de4895&SegmentContainer=ts&MinSegments=1&BreakOnNonKeyFrames=True&h264-profile=high,main,baseline,constrainedbaseline,high10&h264-level=51", video.ID, result.PlaySessionId));
    //        return string.Format("{0}{1}", APIUrl, result.MediaSources.First().TranscodingUrl);
    //        //return string.Format("{0}/videos/{1}/stream?userId={2}&mediaSourceId={3}&playSessionId={4}&static=true", APIUrl, video.ID, GetApiClient().CurrentUserId, result.MediaSources.First().Id, result.PlaySessionId);
    //        //return string.Format("{0}/videos/{1}/master.m3u8?userId={2}&mediaSourceId={3}", APIUrl, video.ID, GetApiClient().CurrentUserId, result.MediaSources.First().Id);
    //        //return string.Format("{0}/videos/786acff9-4b6b-fbe5-9ad2-a2cc5fd4d1de/master.m3u8?DeviceId=X1&MediaSourceId=786acff94b6bfbe59ad2a2cc5fd4d1de&VideoCodec=h264&AudioCodec=mp3,aac&VideoBitrate=119808000&AudioBitrate=192000&PlaySessionId=875cb227593a4964a34a1721b784441f&api_key=c6c98582c31747dc95049c9ba2e5c732&TranscodingMaxAudioChannels=2&RequireAvc=false&Tag=40e66b1d9567d716cfffa9aba5de4895&SegmentContainer=ts&MinSegments=1&BreakOnNonKeyFrames=True&h264-profile=high,main,baseline,constrainedbaseline,high10&h264-level=51", APIUrl);
    //    }
    //    return null;
    //}

    //async Task<VideoData> GetJellyfinVideoDataAsync() {
    //    VideoData result = null;
    //    if (!Testing) {

    //        var capabilities = new ClientCapabilities();
    //        var authResult = await GetApiClient().AuthenticateUserAsync("WPS", "Crazy4Rhinos!");
    //        await GetApiClient().ReportCapabilities(capabilities);
    //        if (string.IsNullOrEmpty(authResult.AccessToken))
    //            throw new Exception("Could not authenticate");
    //        //Trace.Log("Auth: " + authResult.AccessToken);

    //        if (UseCache) {
    //            // Check cache
    //            //Trace.Log("Cache Date: " + Preferences.CDNCacheDate);
    //            if (DateTime.Now.Subtract(Preferences.CDNCacheDate).TotalMinutes < CacheDuration) {
    //                if (!string.IsNullOrEmpty(Preferences.CDNData)) {
    //                    //Trace.Log("Using cached CDN data");
    //                    return JsonConvert.DeserializeObject<VideoData>(Preferences.CDNData);
    //                }
    //            }
    //        }

    //        var items = await GetApiClient().GetItemsAsync(new ItemQuery {
    //            UserId = GetApiClient().CurrentUserId,
    //            ParentId = "6897be232a0274127472d286eb992b77",
    //            Filters = new[] { ItemFilter.IsNotFolder },
    //            IncludeItemTypes = new string[] { "Movie" },
    //            EnableImages = true,
    //            EnableImageTypes = new MediaBrowser.Model.Entities.ImageType[] { MediaBrowser.Model.Entities.ImageType.Primary },
    //            Fields = new ItemFields[] { ItemFields.DateCreated, ItemFields.Overview, ItemFields.SortName, ItemFields.Tags },
    //            Limit = 100,
    //            Recursive = true,
    //            SortBy = new string[] { "SortName" }
    //        });

    //        if (items != null) {
    //            result = new VideoData {
    //                Videos = new List<VideoDataItem>()
    //            };
    //            foreach (var item in items.Items) {
    //                //Trace.Log(JsonConvert.SerializeObject(items));
    //                var videoItem = new VideoDataItem {
    //                    ID = item.Id,
    //                    Name = item.Name,
    //                    Overview = item.Overview
    //                };
    //                if (item.ImageTags != null) {
    //                    if (item.ImageTags.ContainsKey(ImageType.Primary)) {
    //                        videoItem.Thumbnail = string.Format("{0}/Items/{1}/Images/Primary?maxHeight=600&maxWidth=400&tag={2}&quality=90",
    //                            APIUrl, item.Id, item.ImageTags[ImageType.Primary]);
    //                    }
    //                }
    //                if (item.Tags != null && item.Tags.Length > 0) {
    //                    videoItem.Metadata = new List<VideoAttribute>();
    //                    foreach (var tag in item.Tags) {
    //                        ////Trace.Log("Main tag: " + tag);
    //                        var split1 = tag.Split(new string[] { "|" }, StringSplitOptions.None);
    //                        if (split1 != null && split1.Length > 0) {
    //                            foreach (var subTag in split1) {
    //                                ////Trace.Log("Sub tag: " + subTag);
    //                                var split2 = subTag.Split(new string[] { " : " }, StringSplitOptions.None);
    //                                if (split2 != null && split2.Length == 2) {
    //                                    videoItem.Metadata.Add(new VideoAttribute { Name = split2[0].Trim(), Value = split2[1].Trim() });
    //                                }
    //                            }
    //                        }
    //                    }
    //                    //Trace.Log("Metadata: " + JsonConvert.SerializeObject(videoItem.Metadata));
    //                }
    //                result.Videos.Add(videoItem);
    //                //var data = JsonConvert.SerializeObject(item);
    //                ////Trace.Log("Item: " + data);
    //            }
    //        }
    //    } else {
    //        var file = Resources.Load<TextAsset>("test");
    //        //Trace.Log("File: " + file);
    //        if (file != null && !string.IsNullOrEmpty(file.text)) {
    //            result = JsonConvert.DeserializeObject<VideoData>(file.text);
    //        }
    //    }
    //    return result;
    //}

    //async Task<VideoData> GetVidizmoVideoDataAsync() {
    //    //
    //    VideoData result = null;
    //    string data = null;
    //    //
    //    // Vidizmo auth
    //    //
    //    if (!Testing) {
    //        if (UseCache) {
    //            // Check cache
    //            //Trace.Log("Cache Date: " + Preferences.CDNCacheDate);
    //            if (DateTime.Now.Subtract(Preferences.CDNCacheDate).TotalMinutes < CacheDuration) {
    //                if (!string.IsNullOrEmpty(Preferences.CDNData)) {
    //                    //Trace.Log("Using cached CDN data");
    //                    return JsonConvert.DeserializeObject<VideoData>(Preferences.CDNData);
    //                }
    //            }
    //        }
    //        //AccessToken = "NXCK3ZHOHSXOEMJF6APWKH76YXMKEUNKMA27TP3FLPWNSBSUDCKLKDIZ7VMNS4HMTBGYOUEWZHIUJYTDA3RZINOHGGB3AM27VQCMUGV6NTGVXZ4FD2JPVOOTZRNCJ2WDOSNVE6RCBDUKV7RFMPJ5VGT2K6FJGCOAYGCQT5GANGB64JPZ4ADQ";
    //        if (string.IsNullOrEmpty(AccessToken)) {
    //            data = "{\"emailAddress\": \"darryn.britton@gmail.com\", \"password\": \"KrZgSJ8FHcspbzx\", \"tenantId\": 0}";
    //            AccessToken = await Web.Post("https://wildlifeprotectionsolutions.enterprisetube.com/api/v1/user/authenticate", data);
    //        }

    //        if (string.IsNullOrEmpty(AccessToken))
    //            return null;
    //        await Task.Delay(1000);
    //        data = "{\"mashupSearchParts\":[\"BasicInfo\",\"Content\",\"Categories\",\"Participation\",\"Tags\",\"LevelStats\",\"MashupFormatStats\",\"ProcessedStats\",\"AuthorStats\",\"Stats\",\"CustomAttributes\",\"CustomAttributesStats\",\"MashupStatusStats\",\"UserStats\",\"RelatedTags\"],\"orderBy\":\"UpdatedDate\",\"pageSize\":24,\"pageIndex\":0,\"sortType\":\"DESC\",\"status\":[\"Published\"],\"publishingStatus\":[\"CurrentPublished\"],\"mashupFormats\":[\"Video\",\"Audio\",\"Document\",\"Image\"]}";
    //        data = await Web.Post("https://wildlifeprotectionsolutions.enterprisetube.com/api/v1/mashup/search", data, AccessToken);
    //        //Trace.Log("Done posting web request");
    //        if (!string.IsNullOrEmpty(data)) {
    //            result = JsonConvert.DeserializeObject<VideoData>(data);
    //            //result = JsonConvert.DeserializeObject<VideoData>(data);
    //        } else {
    //            //Trace.Log("Failed to retrieve data");
    //        }
    //    } else {
    //        var file = Resources.Load<TextAsset>("test");
    //        //Trace.Log("File: " + file);
    //        if (file != null && !string.IsNullOrEmpty(file.text)) {
    //            result = JsonConvert.DeserializeObject<VideoData>(file.text);
    //        }
    //    }
    //    return result;
    //}

    #endregion

    async Task<VideoData> UpdateYouTubeVideoData(VideoData data) {
        if (data != null && data.Videos != null && data.Videos.Count > 0) {
            List<string> ids = new List<string>();
            foreach (var item in data.Videos) {
                if (item.Metadata != null && item.Metadata.FirstOrDefault(_ => _.Name.Equals("YouTube")) != null) {
                    var value = item.Metadata.FirstOrDefault(_ => _.Name.Equals("YouTube")).Value.Trim().Replace("https://youtu.be/", "");
                    ids.Add(value);
                }
            }
            if (ids.Count == 0)
                return data;
            //Trace.Log("Fetching YouTube videos");
            var youtubeVideos = await YoutubeManager.Instance.LoadVideosAsync(string.Join(",", ids));
            if (youtubeVideos != null && youtubeVideos.Count > 0) {
                UpdateVideoDataFromYoutube(data, youtubeVideos);
                Preferences.YouTubeData = JsonConvert.SerializeObject(youtubeVideos);
                Analytics.TrackEvent(Events.EventVideoListAugmentYouTube, new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
            }
        }
        return data;
    }

    private static void UpdateVideoDataFromYoutube(VideoData data, List<VideoDataItem> youtubeVideos) {
        //Trace.Log(string.Format("Found {0} YouTube videos", youtubeVideos.Count));
        foreach (var youTubeVideo in youtubeVideos) {
            string youTubeID = "";
            foreach (var video in data.Videos) {
                if (video.Metadata != null) {
                    var youTubeAttribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("YouTube"));
                    if (youTubeAttribute != null && !string.IsNullOrEmpty(youTubeAttribute.Value.Trim())) {
                        youTubeID = youTubeAttribute.Value.Trim().Substring(youTubeAttribute.Value.Trim().LastIndexOf("/") + 1);
                        ////Trace.Log(string.Format("Compare YouTube IDs: {0} vs {1}", youTubeID, youTubeVideo.ID));
                        if (youTubeVideo.ID.Equals(youTubeID)) {
                            ////Trace.Log(string.Format("Using stats from YouTube video '{0}' - {1} views", youTubeVideo.ID, youTubeVideo.TotalViews));
                            video.DateCreated = youTubeVideo.DateCreated;
                            video.TotalLikes = youTubeVideo.TotalLikes;
                            video.TotalViews = youTubeVideo.TotalViews;
                            video.TotalDislikes = youTubeVideo.TotalDislikes;
                            if (youTubeVideo.TotalLikes > 0) {
                                ////Trace.Log(string.Format("Calc rating: {0} likes, {1} dislikes, {2} views", youTubeVideo.TotalLikes, youTubeVideo.TotalDislikes, youTubeVideo.TotalViews));
                                var left = youTubeVideo.TotalLikes / ((float)(youTubeVideo.TotalLikes + youTubeVideo.TotalDislikes));
                                ////Trace.Log("Left: " + left);
                                video.Rating = ((left * 100f) / 20f);
                                ////Trace.Log("Rating: " + video.Rating);
                            }
                            //Trace.Log("YouTube: " + youTubeVideo.Name + " - " + JsonConvert.SerializeObject(youTubeVideo));
                            if (!string.IsNullOrEmpty(youTubeVideo.Thumbnail)) {
                                video.Thumbnail = youTubeVideo.Thumbnail;
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    List<Marker> GetMarkers() {
        var markers = new List<Marker>();
        List<string> RegionCache = new List<string>();
        if (Data != null) {
            if (Data.Videos != null) {
                if (m_AllMarker) {
                    m_AllMarker.SetActive(true);
                }
                if (m_SpecialMarker) {
                    m_SpecialMarker.SetActive(true);
                }
                foreach (var result in Data.Videos) {
                    ////Trace.Log(JsonConvert.SerializeObject(result.Metadata));
                    if (result.Metadata != null && result.Metadata.Count > 0) {
                        var regionAttribute = result.Metadata.FirstOrDefault(_ => _.Name.Equals("Portal 1"));
                        if (regionAttribute != null) {
                            //Marker marker = markers.FirstOrDefault(_ => _.Title.Equals(region, System.StringComparison.InvariantCultureIgnoreCase));
                            string[] regions = regionAttribute.Value.Trim().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var region in regions) {
                                //var country = WorldMapGlobe.instance.GetCountry(region);
                                //if (country != null) {
                                if (!RegionCache.Contains(region)) {
                                    // TEMP!
                                    RegionCache.Add(region);
                                    float latitude = 0, longitude = 0;
                                    if (region.Equals("Africa")) {
                                        latitude = -9.809050f;
                                        longitude = 29.134136f;
                                    } else if (region.Equals("Asia")) {
                                        latitude = 9.782303f;
                                        longitude = 112.278657f;
                                    } else if (region.Equals("Europe")) {
                                        latitude = 48.1327673f;
                                        longitude = 4.1753323f;
                                    } else if (region.Equals("North America")) {
                                        latitude = 41.102244f;
                                        longitude = -106.568978f;
                                    } else if (region.Equals("Central America")) {
                                        latitude = 9.782305f;
                                        longitude = -84.156874f;
                                    } else if (region.Equals("Antarctic")) {
                                        //latitude = -57.992002f;
                                        //longitude = -57.965478f;
                                        latitude = -43.533549f;
                                        longitude = -30.435558f;
                                    } else if (region.Equals("Oceans and Rivers")) {
                                        latitude = -0.013573f; // Pacific
                                        longitude = -137.77018f; // Pacific
                                        // Add additional markers
                                        markers.Add(new Marker { Title = region, Latitude = -0.013573f, Longitude = -20.96355f, Flag = "" }); // Atlantic
                                        markers.Add(new Marker { Title = region, Latitude = -0.365133f, Longitude = 74.221990f, Flag = "" }); // Indian
                                    }
                                    Trace.Log("Adding marker: " + region);
                                    if (latitude != 0 && longitude != 0) {
                                        markers.Add(new Marker { Title = region, Latitude = latitude, Longitude = longitude, Flag = "" });
                                    }
                                }
                            }
                            //} else {
                            //    //Trace.Log(string.Format("Country '{0}' not found!", region));
                            //}
                        }
                    }
                }
            }
        }
        return markers;
    }

    public void LoadVideos() {
        if (string.IsNullOrEmpty(CurrentRegion))
            return;
        LoadVideos(CurrentRegion);
    }

    public void LoadVideos(string name, bool clearFilter = false) {
        if (!m_Grid || !m_Template)
            return;
        if (m_AllMarker) {
            m_AllMarker.SetActive(false);
        }
        if (m_SpecialMarker) {
            m_SpecialMarker.SetActive(false);
        }
        //Trace.Log("Load Videos: " + name);
        if (clearFilter) {
            FilterSpeciesCategory = new Dictionary<string, bool?>();
            CurrentPrimarySpecies = new Dictionary<string, bool?>();
            FilterPrimarySpecies = new Dictionary<string, bool?>();
            FilterIUCN = new Dictionary<string, bool?>();
            FilterPopulationTrend = new Dictionary<string, bool?>();
        }
        CurrentRegion = name;
        if (CurrentRegion.Equals("AllMarker")) {
            m_Title.GetComponent<TMP_Text>().text = "All Videos";
        } else if (CurrentRegion.Equals("SpecialMarker")) {
            m_Title.GetComponent<TMP_Text>().text = "Compilations";
        } else {
            m_Title.GetComponent<TMP_Text>().text = isOfflineMode ? string.Format("{0} [Offline Mode]", name) : name;
        }
        if (m_EmptyText) {
            m_EmptyText.SetActive(false);
        }
        m_UI.SetActive(true);
        m_VideoCanvas.SetActive(true);
        m_Loading.SetActive(true);
        foreach (Transform child in m_Grid.transform) {
            if (child.tag.Contains("Template"))
                continue;
            //Trace.Log("Destroying: " + child.name);
            Destroy(child.gameObject);
        }
        bool noneSelected = FilterPrimarySpecies.All(_ => !_.Value.HasValue || !_.Value.Value);
        Debug.Log("None selected: " + noneSelected);
        ShowGraphicDeath = bool.Parse(Preferences.GetString("GraphicDeath", "false"));
        ShowGraphicSex = bool.Parse(Preferences.GetString("GraphicSex", "false"));
        int viewCount = 0;
        if (!string.IsNullOrEmpty(CurrentRegion) && Data != null && Data.Videos != null) {
            //Trace.Log(JsonConvert.SerializeObject(Data.Videos));
            Analytics.TrackEvent(string.Format("{0}_{1}", Events.EventVideoPortal, CurrentRegion), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
            CurrentVideos = new List<VideoDataItem>();
            foreach (var video in Data.Videos) {
                bool add = CheckVideoFilterAdd(video, false, noneSelected);
                if (add) {
                    CurrentVideos.Add(video);
                }
            }
            if (m_EmptyText) {
                m_EmptyText.SetActive(false);
            }
            if (Order == SortOrder.MostViewed) {
                CurrentVideos = CurrentVideos.OrderByDescending(_ => _.TotalViews).ToList();
            } else if (Order == SortOrder.TopRated) {
                CurrentVideos = CurrentVideos.OrderByDescending(_ => _.TotalLikes).ToList();
            } else if (Order == SortOrder.Newest) {
                CurrentVideos = CurrentVideos.OrderByDescending(_ => _.DateCreated).ToList();
            } else if (Order == SortOrder.Favorites) {
                CurrentVideos = CurrentVideos.Where(_ => _.IsFavorite).ToList();
            } else if (Order == SortOrder.Downloads) {
                CurrentVideos = CurrentVideos.Where(_ => _.IsDownloaded).ToList();
            }
            foreach (var video in CurrentVideos) {
                if (isOfflineMode && !video.IsDownloaded)
                    continue;
                var item = new VideoItem {
                    ID = video.ID,
                    Title = video.Name,
                    Description = video.Overview,
                    Url = "",
                    Thumbnail = video.Thumbnail,
                    Data = video
                };
                var videoItem = Instantiate(m_Template);
                videoItem.tag = "Clone";
                videoItem.GetComponent<VideoItemView>().Video = item;
                videoItem.transform.SetParent(m_Grid.transform, false);
                videoItem.SetActive(true);
                viewCount++;
            }
            try {
                LoadFilterData(CurrentVideos);
            } catch (Exception e) {
                Trace.LogError(e.ToString());
            }
            if (viewCount == 0) {
                if (m_EmptyText) {
                    var text = m_EmptyText.GetComponent<TMP_Text>();
                    if (isOfflineMode) {
                        text.text = "You're in offline mode and have no downloaded videos.\nPlease go online to view video.";
                    } else {
                        text.text = "No videos for the specified filter.\nPlease select fewer filter options.";
                    }
                    m_EmptyText.SetActive(true);
                }
            }
        } else {
            Trace.Log("Region is blank or Video data is missing.");
        }
        if (m_LeftArrow != null && m_RightArrow != null) {
            if (viewCount <= 6) {
                //Trace.Log("Hide Arrows");
                m_LeftArrow.SetActive(false);
                m_RightArrow.SetActive(false);
            } else {
                //Trace.Log("Show Arrows");
                m_LeftArrow.SetActive(true);
                m_RightArrow.SetActive(true);
            }
        }
        if (m_ScrollPanel) {
            m_ScrollPanel.horizontalNormalizedPosition = 0;
        }
        //StartCoroutine(LoadThumbnails());
        m_Loading.SetActive(false);
    }

    private bool CheckVideoFilterAdd(VideoDataItem video, bool skipPrimarySpecies = false, bool noneSelected = false) {
        bool add = false;
        if (video.Metadata != null && video.Metadata.Count > 0) {
            var liveAttribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Live"));
            if (liveAttribute == null || liveAttribute.Value.Trim().ToUpper().Equals("N"))
                return false;
            var graphicAttribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Graphic"));
            if (graphicAttribute != null) {
                if (!ShowGraphicDeath && graphicAttribute.Value.Trim().ToLower().Contains("death")) {
                    return false;
                }
                if (!ShowGraphicSex && graphicAttribute.Value.Trim().ToLower().Contains("sex")) {
                    return false;
                }
            }
            var region1Attribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Portal 1"));
            var region2Attribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Portal 2"));
            if (CurrentRegion.Equals("AllMarker") ||
                (region1Attribute != null && region1Attribute.Value.Trim().Contains(CurrentRegion)) ||
                (region2Attribute != null && region2Attribute.Value.Trim().Contains(CurrentRegion))) {
                // Default add
                add = true;
                if (Events.Instance.IsFilterActive && !skipPrimarySpecies) {
                    add = FilterPrimarySpecies != null && FilterPrimarySpecies.Count > 0 && FilterPrimarySpecies.Any(_ => _.Value.HasValue && _.Value.Value);
                }
                // Check filters (to remove "add" via filter)
                var speciesAttribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Colloquial Species Name"));
                if (add && !skipPrimarySpecies && FilterPrimarySpecies != null && FilterPrimarySpecies.Count > 0) {
                    //if (FilterPrimarySpecies.Any(_ => _.Value.HasValue && _.Value.Value)) {
                    if (noneSelected) {
                        //if (speciesAttribute == null || !FilterPrimarySpecies.ContainsKey(speciesAttribute.Value.Trim()) ||
                        //    (speciesAttribute != null && FilterPrimarySpecies.ContainsKey(speciesAttribute.Value.Trim()) &&
                        //    (!FilterPrimarySpecies[speciesAttribute.Value.Trim()].HasValue || FilterPrimarySpecies[speciesAttribute.Value.Trim()].Value == false))) {
                        //    add = false;
                        //}
                        add = speciesAttribute != null && FilterPrimarySpecies.ContainsKey(speciesAttribute.Value.Trim());
                    } else {
                        if (speciesAttribute == null || !FilterPrimarySpecies.ContainsKey(speciesAttribute.Value.Trim()) ||
                            (speciesAttribute != null && FilterPrimarySpecies.ContainsKey(speciesAttribute.Value.Trim()) &&
                            (!FilterPrimarySpecies[speciesAttribute.Value.Trim()].HasValue || FilterPrimarySpecies[speciesAttribute.Value.Trim()].Value == false))) {
                            add = false;
                        }
                    }
                    //}
                }
                if (add && FilterIUCN != null && FilterIUCN.Count > 0) {
                    if (FilterIUCN.Any(_ => _.Value.HasValue && _.Value.Value)) {
                        var iucnAttribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("IUCN"));
                        //Debug.Log(speciesAttribute.Value + " IUCN - Attr: " + JsonConvert.SerializeObject(iucnAttribute) + "Val: " + JsonConvert.SerializeObject(FilterIUCN[iucnAttribute.Value]));
                        if (iucnAttribute != null && FilterIUCN.ContainsKey(iucnAttribute.Value.Trim()) &&
                            (!FilterIUCN[iucnAttribute.Value.Trim()].HasValue || FilterIUCN[iucnAttribute.Value.Trim()].Value == false)) {
                            add = false;
                        }
                    }
                }
                if (add) {
                    //Debug.Log("Add after IUCN!");
                }
                if (add && FilterPopulationTrend != null && FilterPopulationTrend.Count > 0) {
                    if (FilterPopulationTrend.Any(_ => _.Value.HasValue && _.Value.Value)) {
                        var populationAttribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Population Trend"));
                        if (populationAttribute != null && FilterPopulationTrend.ContainsKey(populationAttribute.Value.Trim()) &&
                            (!FilterPopulationTrend[populationAttribute.Value.Trim()].HasValue || FilterPopulationTrend[populationAttribute.Value.Trim()].Value == false)) {
                            add = false;
                        }
                    }
                }
            } else if (CurrentRegion.Equals("SpecialMarker")) {
                var compilationAttribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Compilations"));
                if (compilationAttribute != null && compilationAttribute.Value.Trim().Equals("Y")) {
                    add = true;
                }
            }
        }
        return add;
    }

    public void UpdateVideoGalleryThumbnailHoverState() {
        foreach (Transform child in m_Grid.transform) {
            if (child.tag.Contains("Template"))
                continue;
            var details = child.gameObject.FindObject("VideoItemDetails");
            details.SetActive(false);
        }
    }

    public void UpdateVideosDownloadStatus() {
        if (!m_Grid)
            return;
        try {
            foreach (Transform child in m_Grid.transform) {
                if (child.tag.Contains("Template"))
                    continue;
                var video = child.GetComponent<VideoItemView>();
                var downloadImage = video.Download.gameObject.FindObject("Image").GetComponent<Image>();
                if (Events.downloadBusy) {
                    if (video.Video.Data.ID != Events.activeDownload.Video.Data.ID) {
                        if (!video.Video.Data.IsDownloaded) {
                            downloadImage.sprite = Resources.Load<Sprite>("ic_gallery_download_blocked");
                        }
                        video.Download.GetComponent<VRInteractiveItem>().IsGazeable = false;
                    }
                } else {
                    downloadImage.sprite = video.Video.Data.IsDownloaded ? Resources.Load<Sprite>("ic_gallery_download_on") : Resources.Load<Sprite>("ic_gallery_download_off");
                    video.Download.GetComponent<VRInteractiveItem>().IsGazeable = !video.Video.Data.IsDownloaded;
                }
            }
        } catch (Exception e) {
            Trace.LogError(e.ToString());
        }
    }

    IEnumerator LoadThumbnailCache() {
        Trace.Log("Start: Loading thumbnail cache");
        foreach (var video in Data.Videos) {
            yield return Main.Graphics.Cache(video);
        }
        Trace.Log("End: Loading thumbnail cache");
        //m_Loading.SetActive(false);
    }

    IEnumerator LoadThumbnails() {
        foreach (Transform child in m_Grid.transform) {
            if (child.tag.Contains("Template"))
                continue;
            var view = child.GetComponent<VideoItemView>();
            if (view) {
                yield return Main.Graphics.FromCache(view, true, (item) => {
                    try {
                        view.Thumbnail.sprite = item.Sprite;
                    } catch (Exception e) {
                        // ???
                        Trace.LogError(e.ToString());
                        Trace.Log(view.Thumbnail.ToString());
                    }
                });
            }
        }
    }

    IEnumerator LoadThumbnail(Transform item) {
        var view = item.GetComponent<VideoItemView>();
        if (view) {
            yield return Main.Graphics.FromCache(view, true, (callback) => {
                try {
                    callback.View.Thumbnail.sprite = callback.Sprite;
                } catch (Exception e) {
                    // ???
                    Trace.LogError(e.ToString());
                }
            });
        }
    }

    public void LoadFilterData() {
        FilterRegion = null;
        LoadVideos(CurrentRegion, true);
    }

    public void SetFilterOn() {
        if (m_FilterButton) {
            var button = m_FilterButton.GetComponent<Button>();
            var text = m_FilterButton.FindObject("Text").GetComponent<TMP_Text>();
            var image = button.GetComponent<Image>();
            image.color = new Color32(0, 157, 255, 255);
            text.text = "Active Filter";
        }
    }

    public void LoadFilterData(List<VideoDataItem> videos) {
        if (videos == null || videos.Count == 0)
            return;
        if (!m_FilterPanel)
            return;
        Trace.Log("Load filter: check region - " + CurrentRegion + " = " + FilterRegion);
        if (CurrentRegion == FilterRegion)
            return;
        if (m_FilterButton) {
            var button = m_FilterButton.GetComponent<Button>();
            var text = m_FilterButton.FindObject("Text").GetComponent<TMP_Text>();
            var image = button.GetComponent<Image>();
            image.color = new Color32(255, 255, 255, 255);
            text.text = "Filter";
        }
        Trace.Log("Loading filter data for region: " + CurrentRegion);
        FilterRegion = CurrentRegion;

        // Load category filters
        LoadFilterDataCategory(videos, "SpeciesCategoryFilterContent", "SpeciesCategoryFilterTemplate", "Species Category", ref FilterSpeciesCategory, true, true);
        LoadFilterPrimarySpeciesDataCategory(videos, "PrimarySpeciesFilterContent", "PrimarySpeciesFilterTemplate", "Colloquial Species Name", ref FilterPrimarySpecies, true, true);
        LoadIUCNFilterDataCategory(videos);
        LoadFilterDataCategory(videos, "PopulationTrendFilterContent", "PopulationTrendFilterTemplate", "Population Trend", ref FilterPopulationTrend, false, false);

        // Store current species for checking/unchecking filters
        CurrentPrimarySpecies = FilterPrimarySpecies;
        Debug.Log(JsonConvert.SerializeObject(CurrentPrimarySpecies));
    }

    public void LoadIUCNFilterDataCategory(List<VideoDataItem> videos) {
        var list = GetIUCNFilterAttributeItems(videos);
        LoadFilterDataCategory(videos, "IUCNFilterContent", "IUCNFilterTemplate", list, ref FilterIUCN, false);
    }

    private List<string> GetIUCNFilterAttributeItems(List<VideoDataItem> videos) {
        Dictionary<string, bool> list = new Dictionary<string, bool> {
            { "Extinct (EX)", false },
            { "Extinct In The Wild (EW)", false },
            { "Critically Endangered (CR)", false },
            { "Endangered (EN)", false },
            { "Vulnerable (VU)", false },
            { "Near Threatened (NT)", false },
            { "Least Concern (LC)", false },
            { "Data Deficient (DD)", false },
            { "Not Evaluated (NE)", false }
        };
        foreach (var video in videos) {
            if (video.Metadata != null && video.Metadata.Count > 0) {
                var attribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("IUCN"));
                if (attribute != null && list.ContainsKey(attribute.Value.Trim())) {
                    list[attribute.Value.Trim()] = true;
                }
            }
        }
        return list.Where(_ => _.Value).Select(_ => _.Key).ToList();
    }

    public void LoadFilterDataCategory(List<VideoDataItem> videos, string gridName, string templateName, string metadata, ref Dictionary<string, bool?> filter, bool sort, bool on) {
        var list = GetFilterAttributeItems(videos, metadata);
        if (sort) {
            list.Sort();
        }
        LoadFilterDataCategory(videos, gridName, templateName, list, ref filter, on);
    }

    public void LoadFilterPrimarySpeciesDataCategory(List<VideoDataItem> videos, string gridName, string templateName, string metadata, ref Dictionary<string, bool?> filter, bool sort, bool on) {
        var list = GetFilterAttributePrimarySpeciesItems(videos, metadata);
        if (sort) {
            list.Sort();
        }
        LoadFilterDataCategory(videos, gridName, templateName, list, ref filter, on);
    }

    public void LoadFilterDataCategory(List<VideoDataItem> videos, string gridName, string templateName, List<string> items, ref Dictionary<string, bool?> filter, bool on) {
        if (videos == null || videos.Count == 0 || string.IsNullOrEmpty(gridName) || string.IsNullOrEmpty(templateName) || items == null)
            return;
        // Get filter grid
        var grid = m_FilterPanel.FindObject(gridName).GetComponent<GridLayoutGroup>();
        foreach (Transform child in grid.transform) {
            if (child.tag.Contains("Template"))
                continue;
            Destroy(child.gameObject);
        }
        var template = m_FilterPanel.FindObject(templateName);
        template.SetActive(false);
        filter = new Dictionary<string, bool?>();
        foreach (var item in items) {
            filter.Add(item.Trim(), null);
        }
        foreach (var filterKey in filter.Keys) {
            var filterItem = Instantiate(template);
            var item = filterItem.GetComponent<Toggle>();
            var label = filterItem.FindObject("Label").GetComponent<TMP_Text>();
            item.isOn = on;
            label.text = filterKey.Trim();
            filterItem.name = filterKey.Trim();
            filterItem.tag = "Clone";
            filterItem.transform.SetParent(grid.transform, false);
            filterItem.SetActive(true);
        }
    }

    private List<string> GetFilterAttributeItems(List<VideoDataItem> videos, string metadata) {
        List<string> list = new List<string>();
        foreach (var video in videos) {
            if (video.Metadata != null && video.Metadata.Count > 0) {
                var attribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals(metadata));
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value.Trim()) && !list.Contains(attribute.Value.Trim())) {
                    list.Add(attribute.Value.Trim());
                }
            }
        }
        return list;
    }

    private List<string> GetFilterAttributePrimarySpeciesItems(List<VideoDataItem> videos, string metadata) {
        List<string> list = new List<string>();
        foreach (var video in videos) {
            if (video.Metadata != null && video.Metadata.Count > 0) {
                var attribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals(metadata));
                //Debug.Log("GetFilterAttributePrimarySpeciesItems: " + metadata + " - " + attribute.Value.Trim());
                bool add = CheckVideoFilterAdd(video, true);
                if (add && attribute != null && !string.IsNullOrEmpty(attribute.Value.Trim()) && !list.Contains(attribute.Value.Trim())) {
                    list.Add(attribute.Value.Trim());
                }
            }
        }
        return list;
    }

    public void UpdateSpeciesCategoryFilterDataCategory(bool clear, bool deselectAll = false) {
        Trace.Log("UpdateSpeciesCategoryFilterDataCategory");
        if (Data.Videos == null || Data.Videos.Count == 0)
            return;
        // Get species category filter grid
        var categories = new List<string>();
        var categoryGrid = m_FilterPanel.FindObject("SpeciesCategoryFilterContent").GetComponent<GridLayoutGroup>();
        foreach (Transform child in categoryGrid.transform) {
            if (child.tag.Contains("Template"))
                continue;
            var item = child.gameObject.GetComponent<Toggle>();
            var label = child.gameObject.FindObject("Label").GetComponent<TMP_Text>();
            if (item.isOn || clear) {
                categories.Add(label.text);
            }
        }
        // Check ignore
        //if (categories.Count == 0)
        //    return;
        // Get primary species filter grid
        var grid = m_FilterPanel.FindObject("PrimarySpeciesFilterContent").GetComponent<GridLayoutGroup>();
        foreach (Transform child in grid.transform) {
            if (child.tag.Contains("Template"))
                continue;
            //var item = child.gameObject.GetComponent<Toggle>();
            //var label = child.gameObject.FindObject("Label").GetComponent<TMP_Text>();
            ////if (item.isOn) {
            //    FilterPrimarySpecies[label.text] = item.isOn;
            ////}
            Destroy(child.gameObject);
        }
        var template = m_FilterPanel.FindObject("PrimarySpeciesFilterTemplate");
        template.SetActive(false);
        FilterPrimarySpecies = new Dictionary<string, bool?>();
        foreach (var video in Data.Videos) {
            if (video.Metadata != null && video.Metadata.Count > 0) {
                var region1Attribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Portal 1"));
                var region2Attribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Portal 2"));
                var categoryAttribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Species Category"));
                var speciesAttribute = video.Metadata.FirstOrDefault(_ => _.Name.Equals("Colloquial Species Name"));
                if (CurrentRegion.Equals("AllMarker") || (region1Attribute != null && region1Attribute.Value.Trim().Contains(CurrentRegion)) ||
                    (region2Attribute != null && region2Attribute.Value.Trim().Contains(CurrentRegion))) {
                    if (speciesAttribute != null && !string.IsNullOrEmpty(speciesAttribute.Value.Trim()) &&
                        !FilterPrimarySpecies.ContainsKey(speciesAttribute.Value.Trim()) &&
                        categoryAttribute != null && categories.Contains(categoryAttribute.Value.Trim())) {
                        // Add stub
                        bool add = CheckVideoFilterAdd(video, true);
                        if (add) {
                            bool? isOn = null;
                            //bool existing = CurrentPrimarySpecies.ContainsKey(speciesAttribute.Value.Trim());
                            //bool? active = null;
                            if (CurrentPrimarySpecies.ContainsKey(speciesAttribute.Value.Trim())) {
                                isOn = CurrentPrimarySpecies[speciesAttribute.Value.Trim()];
                            }
                            //if (existing && active == null) {
                            //    isOn = true;
                            //}
                            if (isOn == null) {
                                isOn = true;
                            }
                            if (deselectAll) {
                                isOn = false;
                            }
                            //Debug.Log("Existing/Active: " + speciesAttribute.Value + " - " + isOn);
                            //FilterPrimarySpecies.Add(speciesAttribute.Value.Trim(), existing && !deselectAll ? true : (bool?)null);
                            FilterPrimarySpecies.Add(speciesAttribute.Value.Trim(), isOn);
                        }
                    }
                }
            }
        }
        var keysList = FilterPrimarySpecies.Keys.ToList();
        keysList.Sort();
        foreach (var key in keysList) {
            var filterItem = Instantiate(template);
            var label = filterItem.FindObject("Label").GetComponent<TMP_Text>();
            label.text = key;
            filterItem.name = key;
            filterItem.tag = "Clone";
            filterItem.transform.SetParent(grid.transform, false);
            filterItem.SetActive(true);
            var item = filterItem.gameObject.GetComponent<Toggle>();
            item.isOn = FilterPrimarySpecies[key].HasValue && FilterPrimarySpecies[key].Value;
        }
        Debug.Log("Keys List: " + JsonConvert.SerializeObject(keysList));
    }

    public class TokenAuth {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }

    public class Marker : MonoBehaviour {
        public string Title { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public string Flag { get; set; }
    }

    public class VideoData {
        [JsonProperty("Items")]
        public List<VideoDataItem> Videos { get; set; }
    }

    //public class VideoThumbnail {
    //    public string ThumbnailURL { get; set; }
    //    public int ThumbnailSize { get; set; }
    //}

    public class VideoContent {
        public string ID { get; set; }
        public VideoContentDetails ContentDetails { get; set; }
    }

    public class VideoContentDetails {
        public List<VideoContentFile> ContentFiles { get; set; }
    }

    public class VideoContentFile {
        public string PlaybackURL { get; set; }
        public string DownloadURL { get; set; }
        public List<VideoAttribute> ContentFileAttributes { get; set; }
    }

    public class VideoStats {
        public ulong TotalViews { get; set; }
        public ulong TotalLikes { get; set; }
        public ulong TotalDislikes { get; set; }
        public float Rating { get; set; }
    }

    public class VideoCustomAttribute {
        public string ID { get; set; }
        [JsonProperty("customAttributeName")]
        public string Name { get; set; }
        [JsonProperty("customAttributeValue")]
        public string Value { get; set; }
    }

    public class AzureAssetList {
        [JsonProperty("value")]
        public List<AzureAsset> Assets { get; set; }
    }

    public class AzureAsset {
        public string ID { get; set; }
        public string Name { get; set; }
        public AzureAssetProperties Properties { get; set; }
    }

    public class AzureAssetProperties {
        public string AssetID { get; set; }
        public string Description { get; set; }
        public string Container { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModified { get; set; }
    }

    public class AzureStreamingLocators {
        public List<AzureStreamingLocator> StreamingLocators { get; set; }
    }

    public class AzureStreamingLocator {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("streamingLocatorId")]
        public string StreamingLocatorID { get; set; }
    }

    public class AzureStreamingPaths {
        [JsonProperty("streamingPaths")]
        public List<AzureStream> Streams { get; set; }
    }

    public class AzureStream {
        [JsonProperty("streamingProtocol")]
        public string Protocol { get; set; }
        public List<string> Paths { get; set; }
    }

    public class AzureContainerURLs {
        [JsonProperty("assetContainerSasUrls")]
        public List<string> URLs { get; set; }
    }

    public class VideoFavorites {
        public List<VideoFavorite> Favorites { get; set; }
        public VideoFavorites() {
            Favorites = new List<VideoFavorite>();
        }
    }

    public class VideoFavorite {
        public string ID { get; set; }
        public bool IsFavorite { get; set; }
    }
}
