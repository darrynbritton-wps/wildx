﻿using UnityEngine;
using static AppLoader;

namespace Assets.Scripts {
    public class VideoItem : MonoBehaviour {

        public string ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Thumbnail { get; set; }
        public VideoDataItem Data { get; set; }

        public void Awake() {

        }

    }
}
