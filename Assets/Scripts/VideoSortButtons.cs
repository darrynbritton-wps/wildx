﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRStandardAssets.Utils;

public class VideoSortButtons : MonoBehaviour {

    [SerializeField] public GameObject SortMostViewedButton;
    [SerializeField] public GameObject SortTopRatedButton;
    [SerializeField] public GameObject SortNewestButton;
    [SerializeField] public GameObject SortFavoritesButton;
    [SerializeField] public GameObject SortDownloadsButton;

    public AppLoader.SortOrder PreviousSortOrder = AppLoader.SortOrder.TopRated;

    void Start() {
        //if (SortMostViewedButton) {
        //    SetButtonActive(SortMostViewedButton);
        //}
    }

    public void SetButtonActive(GameObject button) {
        var image = button.GetComponent<Image>();
        var interactible = button.GetComponent<VRInteractiveItem>();
        image.color = new Color32(0, 157, 255, 255);
        if (AppLoader.Instance != null) {
            PreviousSortOrder = AppLoader.Instance.Order;
            if (button == SortMostViewedButton) {
                AppLoader.Instance.Order = AppLoader.SortOrder.MostViewed;
                SetButtonInactive(SortTopRatedButton);
                SetButtonInactive(SortNewestButton);
                SetButtonInactive(SortFavoritesButton);
                SetButtonInactive(SortDownloadsButton);
                interactible.IsGazeable = false;
            } else if (button == SortTopRatedButton) {
                AppLoader.Instance.Order = AppLoader.SortOrder.TopRated;
                SetButtonInactive(SortMostViewedButton);
                SetButtonInactive(SortNewestButton);
                SetButtonInactive(SortFavoritesButton);
                SetButtonInactive(SortDownloadsButton);
                interactible.IsGazeable = false;
            } else if (button == SortNewestButton) {
                AppLoader.Instance.Order = AppLoader.SortOrder.Newest;
                SetButtonInactive(SortMostViewedButton);
                SetButtonInactive(SortTopRatedButton);
                SetButtonInactive(SortFavoritesButton);
                SetButtonInactive(SortDownloadsButton);
                interactible.IsGazeable = false;
            } else if (button == SortFavoritesButton) {
                AppLoader.Instance.Order = AppLoader.SortOrder.Favorites;
                SetButtonInactive(SortMostViewedButton);
                SetButtonInactive(SortNewestButton);
                SetButtonInactive(SortTopRatedButton);
                SetButtonInactive(SortDownloadsButton);
            } else if (button == SortDownloadsButton) {
                AppLoader.Instance.Order = AppLoader.SortOrder.Downloads;
                SetButtonInactive(SortMostViewedButton);
                SetButtonInactive(SortTopRatedButton);
                SetButtonInactive(SortNewestButton);
                SetButtonInactive(SortFavoritesButton);
            }
            AppLoader.Instance.LoadVideos();
        }
    }

    public void SetButtonInactive(GameObject button) {
        var image = button.GetComponent<Image>();
        var interactible = button.GetComponent<VRInteractiveItem>();
        image.color = new Color32(255, 255, 255, 255);
        interactible.IsGazeable = true;
    }
}
