using UnityEngine;
using UnityEngine.Animations;

using WPM;

namespace Assets.Scripts {
    public class InteractiveGlobeRotationItem : InteractiveItem {

        [SerializeField] private WorldMapGlobe m_Globe;
        [SerializeField] private Axis m_Axis;
        [SerializeField] private bool m_Positive;
        protected bool _isRotating;
        private float _speed = 0.3f;
        private float _damping = 0.001f;
        public static float CurrentY = 0;
        private const int deltaY = 150;

        public new void Awake() {
        }

        protected override void HandleOver(RaycastHit hit, GameObject item) {
            _hitPosition = hit.point;
            _isRotating = true;
        }

        protected override void HandleMove(RaycastHit hit, GameObject item) {
            _hitPosition = hit.point;
            _isRotating = true;
        }

        protected override void HandleOut(GameObject item) {
            _isRotating = false;
            _hitPosition = Vector3.zero;
        }

        protected override void HandleGazed(RaycastHit hit, GameObject item) {

        }

        protected override void HandleClick() {

        }

        protected override void HandleDoubleClick() {

        }

        public new void Update() {
            if (_isRotating) {
                if (m_Axis == Axis.X) {
                    m_Globe.transform.rotation = Quaternion.Euler(0, m_Positive ? _speed : -_speed, 0) * m_Globe.transform.rotation;
                } else {
                    float y = m_Positive ? _speed : -_speed;
                    if (m_Positive && CurrentY >= -deltaY && CurrentY < deltaY) {
                        m_Globe.transform.rotation = Quaternion.Euler(y, 0, 0) * m_Globe.transform.rotation;
                        if (m_Positive) {
                            CurrentY = CurrentY + 1;
                            if (CurrentY > deltaY)
                                CurrentY = deltaY;
                        } else {
                            CurrentY = CurrentY - 1;
                            if (CurrentY < -deltaY)
                                CurrentY = -deltaY;
                        }
                    }
                    if (!m_Positive && CurrentY <= deltaY && CurrentY > -deltaY) {
                        m_Globe.transform.rotation = Quaternion.Euler(y, 0, 0) * m_Globe.transform.rotation;
                        if (m_Positive) {
                            CurrentY = CurrentY + 1;
                            if (CurrentY > deltaY)
                                CurrentY = deltaY;
                        } else {
                            CurrentY = CurrentY - 1;
                            if (CurrentY < -deltaY)
                                CurrentY = -deltaY;
                        }
                    }
                }
            }
        }
    }
}