﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Xml;

using Assets.Scripts;
using Assets.Scripts.Utils;

using ControllerSelection;

using Microsoft.AppCenter.Unity.Analytics;
using Microsoft.AppCenter.Unity.Crashes;

using Newtonsoft.Json;

using RenderHeads.Media.AVProVideo;

using TMPro;

using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;

using VRStandardAssets.Utils;

public class Events : MonoBehaviour {

    public static Events Instance;
    public static string CurrentVideo;
    public static bool VideoPlaying = false;

    private MediaPlayer mediaPlayer;
    private Image mediaProgress;
    private Slider mediaSeekSlider;
    private TMP_Text mediaSeekCurrent, mediaSeekEnd;
    private VideoItemView CurrentVideoItem;

    //[SerializeField] private WorldMapGlobe m_Globe;
    //[SerializeField] private GameObject m_Reticle;
    //[SerializeField] private GameObject m_Menu;
    //[SerializeField] private YoutubePlayer.YoutubePlayer m_Player;
    [SerializeField] private Material DefaultSkybox;
    [SerializeField] private Material VideoSkybox;
    [SerializeField] private GameObject VideoInfo;
    [SerializeField] public GameObject m_UI;
    [SerializeField] public GameObject m_Loading;
    [SerializeField] public GameObject m_SettingsUI;
    [SerializeField] public GameObject m_ErrorUI;
    [SerializeField] public GameObject m_DebugUI;
    [SerializeField] public GameObject m_FilterUI;
    [SerializeField] public GameObject m_TutorialUI;
    [SerializeField] public GameObject m_ConfirmUI;
    [SerializeField] public GameObject m_VideoMenu;
    [SerializeField] public GameObject m_AllMarker;
    [SerializeField] public GameObject m_SpecialMarker;
    [SerializeField] public GameObject m_SpeciesCategoryToggleAll;
    [SerializeField] public GameObject m_SpeciesPrimaryToggleAll;
    [SerializeField] public GameObject m_GameDriveUI;

    public static string EventAppStart = "AppStart";
    public static string EventAppClose = "AppClose";
    public static string EventAuthAzure = "AuthAzure";
    public static string EventVideoListLoadAzure = "VideoListLoadAzure";
    public static string EventVideoListLoadCache = "VideoListLoadCache";
    public static string EventVideoListAugmentYouTube = "VideoListAugmentYouTube";
    public static string EventVideoPortal = "VideoPortal";
    public static string EventVideoPlay = "VideoPlay";
    public static string EventVideoDownload = "VideoDownload";
    public static string EventVideoFavorite = "VideoFavorite";
    public static string EventVideoDelete = "VideoDelete";
    public static string EventVideoUnfavorite = "VideoUnfavorite";

    public string TutorialText;
    public bool IsFilterActive;

    private bool videoPausedByInfo;
    private VideoItemView DeleteVideo;
    private const float horizontalScrollSpeed = 0.8f;
    private const float verticalScrollSpeed = 0.3f;
    private GameDrive CurrentGameDrive = null;
    private GameObject GameDriveCanvas;
    private GameObject GameDriveSphere;
    private OVRRawRaycaster vRRawRaycaster;

    public class EventTraceProperties {
        public Dictionary<string, string> Properties { get; set; }

        public EventTraceProperties() {
        }

        public EventTraceProperties Add(Dictionary<string, string> values) {
            if (Properties == null) {
                Properties = new Dictionary<string, string>();
            }
            foreach (var key in values.Keys) {
                if (!Properties.ContainsKey(key)) {
                    Properties.Add(key, values[key]);
                }
            }
            return this;
        }
    }

    public static Dictionary<string, string> GetProperties() {
        Dictionary<string, string> result = new Dictionary<string, string> {
            { "Version", Application.version },
            { "Platform", Application.platform.ToString() },
            { "DeviceModel", SystemInfo.deviceModel },
            { "DeviceName", SystemInfo.deviceName },
            { "DeviceType", SystemInfo.deviceType.ToString() },
            { "DeviceID", SystemInfo.deviceUniqueIdentifier }
        };
        return result;
    }

    public static Dictionary<string, string> GetOculusProperties() {
        Dictionary<string, string> result = new Dictionary<string, string> {
            { "OculusUser", OVRManager.profile != null ? OVRManager.profile.name : "Unknown" },
            { "OculusDevice", OVRPlugin.productName }
        };
        return result;
    }

    public void Start() {

    }

    public void OnApplicationQuit() {
        Analytics.TrackEvent(EventAppClose, new EventTraceProperties().Add(GetProperties()).Add(GetOculusProperties()).Properties);
    }

    public void Awake() {
        Instance = this;
        Analytics.TrackEvent(EventAppStart, new EventTraceProperties().Add(GetProperties()).Add(GetOculusProperties()).Properties);
        Application.logMessageReceived += Application_logMessageReceived;
        if (m_DebugUI) {
            m_DebugUI.SetActive(Trace.ShowDebugWindow);
        }
    }

    private void Application_logMessageReceived(string condition, string stackTrace, LogType type) {
        if (type == LogType.Exception) {
            Crashes.TrackError(new Exception(string.Format("{0}\n{1}", condition, stackTrace)), new EventTraceProperties().Add(GetProperties()).Add(GetOculusProperties()).Properties);
        }
    }

    public void ResetGlobe(GameObject globe) {
        if (!globe)
            return;
        globe.transform.rotation = new Quaternion(0f, -1f, 0f, 0.2f);
        InteractiveGlobeRotationItem.CurrentY = 0;
    }

    public void ScrollRectRight(ScrollRect rect) {
        if (!rect)
            return;
        //rect.horizontalNormalizedPosition += 0.005f;
        var diff = rect.content.rect.width - rect.viewport.rect.width;
        if (diff > 0) {
            rect.horizontalNormalizedPosition += horizontalScrollSpeed / diff;
            //Trace.Log(diff.ToString());
        }
    }

    public void ScrollRectLeft(ScrollRect rect) {
        if (!rect)
            return;
        var diff = rect.content.rect.width - rect.viewport.rect.width;
        if (diff > 0) {
            rect.horizontalNormalizedPosition -= horizontalScrollSpeed / diff;
            //Trace.Log(diff.ToString());
        }
    }

    public void ScrollRectDown(ScrollRect rect) {
        if (!rect)
            return;
        var diff = rect.content.rect.height - rect.viewport.rect.height;
        if (diff > 0) {
            rect.verticalNormalizedPosition -= verticalScrollSpeed / diff;
            //Trace.Log(diff.ToString());
        }
        //if (rect.content.rect.height < 200) {
        //    rect.verticalNormalizedPosition -= 0.02f;
        //} else {
        //    rect.verticalNormalizedPosition -= 0.005f;
        //}
    }

    public void ScrollRectUp(ScrollRect rect) {
        if (!rect)
            return;
        var diff = rect.content.rect.height - rect.viewport.rect.height;
        if (diff > 0) {
            rect.verticalNormalizedPosition += verticalScrollSpeed / diff;
            //Trace.Log(diff.ToString());
        }
    }

    public void ShowFilterUI() {
        if (!m_FilterUI)
            return;
        m_FilterUI.SetActive(true);
        IsFilterActive = true;
    }

    public void HideFilterUI() {
        if (!m_FilterUI)
            return;
        m_FilterUI.SetActive(false);
        IsFilterActive = false;
    }

    public void CloseUI(GameObject ui) {
        if (!ui)
            return;
        //Trace.Log("CloseUI: " + ui.name);
        ui.SetActive(false);
    }

    public void CloseGalleryUI() {
        if (!m_UI)
            return;
        m_UI.SetActive(false);
        if (m_FilterUI && m_FilterUI.activeInHierarchy) {
            HideFilterUI();
        }
        if (m_AllMarker) {
            m_AllMarker.SetActive(true);
        }
        if (m_SpecialMarker) {
            m_SpecialMarker.SetActive(true);
        }
    }

    public void VideoGalleryHoverIn(GameObject item) {
        AppLoader.Instance.UpdateVideoGalleryThumbnailHoverState();
        item.SetActive(true);
    }

    public void VideoGalleryHoverOut(GameObject item) {
        item.SetActive(false);
    }

    public void ShowError(string message) {
        if (!m_ErrorUI)
            return;
        m_ErrorUI.SetActive(true);
        var text = GameObject.Find("/ErrorUI/MainCanvas/Info/Text").GetComponent<TMP_Text>();
        text.text = message;
        Crashes.TrackError(new Exception(message), new EventTraceProperties().Add(GetProperties()).Add(GetOculusProperties()).Properties);
    }

    public void ShowUI(GameObject ui) {
        if (!ui)
            return;
        //Trace.Log("ShowUI: " + ui.name);
        ui.SetActive(true);
    }

    public void ShowSettings() {
        if (!m_SettingsUI)
            return;
        m_SettingsUI.SetActive(true);
        var toggle1 = GameObject.Find("/SettingsUI/MainCanvas/VideoInfo/Toggles/GraphicDeathToggle").GetComponent<Toggle>();
        var toggle2 = GameObject.Find("/SettingsUI/MainCanvas/VideoInfo/Toggles/GraphicSexToggle").GetComponent<Toggle>();
        toggle1.isOn = bool.Parse(Main.Preferences.GetString("GraphicDeath", "false"));
        toggle2.isOn = bool.Parse(Main.Preferences.GetString("GraphicSex", "false"));
    }

    public void UpdateSettings() {
        var toggle1 = GameObject.Find("/SettingsUI/MainCanvas/VideoInfo/Toggles/GraphicDeathToggle").GetComponent<Toggle>();
        var toggle2 = GameObject.Find("/SettingsUI/MainCanvas/VideoInfo/Toggles/GraphicSexToggle").GetComponent<Toggle>();
        Main.Preferences.SetString("GraphicDeath", toggle1.isOn.ToString());
        Main.Preferences.SetString("GraphicSex", toggle2.isOn.ToString());
        AppLoader.Instance.LoadVideos();
        m_SettingsUI.SetActive(false);
    }

    public void ShowTutorial() {
        if (!m_TutorialUI || string.IsNullOrEmpty(TutorialText))
            return;
        m_TutorialUI.SetActive(true);
        var content = GameObject.Find("/TutorialUI/MainCanvas/Info/Scroll View/Viewport/Content").GetComponent<TMP_Text>();
        content.text = TutorialText;
        //Main.Preferences.TutorialShown = true;
    }

    public void HideTutorial() {
        var check = GameObject.Find("/TutorialUI/MainCanvas/Info/DoneToggle").GetComponent<Toggle>();
        Main.Preferences.TutorialShown = !check.isOn;
        m_TutorialUI.SetActive(false);
    }

    private bool menuActive = false;
    public void ToggleMenu(GameObject item) {
        if (!item || !m_VideoMenu)
            return;
        var image = item.GetComponent<Image>();
        if (menuActive) {
            image.sprite = Resources.Load<Sprite>("ic_menu");
            menuActive = false;
        } else {
            image.sprite = Resources.Load<Sprite>("ic_menu_close");
            menuActive = true;
        }
        m_VideoMenu.SetActive(menuActive);
    }

    public void ToggleSpeciesCategoryFilterItem(GameObject item) {
        if (!item)
            return;
        var toggle = item.GetComponent<Toggle>();
        if (!toggle)
            return;
        toggle.Select();
        AppLoader.Instance.UpdateSpeciesCategoryFilterDataCategory(false);
        ApplyFilter();
    }

    public void TogglePrimarySpeciesFilterItem(GameObject item) {
        if (!item)
            return;
        var toggle = item.GetComponent<Toggle>();
        if (!toggle)
            return;
        Debug.Log("Toggle start: " + toggle.name + " (" + toggle.isOn + ")");
        //toggle.isOn = !toggle.isOn;
        toggle.Select();
        if (AppLoader.Instance.FilterPrimarySpecies != null) {
            if (AppLoader.Instance.FilterPrimarySpecies.ContainsKey(toggle.name)) {
                AppLoader.Instance.FilterPrimarySpecies[toggle.name] = toggle.isOn;
            }
        }
        if (AppLoader.Instance.CurrentPrimarySpecies != null) {
            if (AppLoader.Instance.CurrentPrimarySpecies.ContainsKey(toggle.name)) {
                Debug.Log("Toggle now: " + toggle.name + " (" + toggle.isOn + ")");
                AppLoader.Instance.CurrentPrimarySpecies[toggle.name] = toggle.isOn;
            }
        }
        //Debug.Log(JsonConvert.SerializeObject(VideoLoader.Instance.CurrentPrimarySpecies));
        AppLoader.Instance.UpdateSpeciesCategoryFilterDataCategory(false);
        ApplyFilter();
    }

    public void ToggleIUCNFilterItem(GameObject item) {
        if (!item)
            return;
        var toggle = item.GetComponent<Toggle>();
        if (!toggle)
            return;
        //toggle.isOn = !toggle.isOn;
        toggle.Select();
        if (AppLoader.Instance.FilterIUCN != null) {
            if (AppLoader.Instance.FilterIUCN.ContainsKey(toggle.name)) {
                AppLoader.Instance.FilterIUCN[toggle.name] = toggle.isOn;
            }
        }
        AppLoader.Instance.UpdateSpeciesCategoryFilterDataCategory(false);
        ApplyFilter();
    }

    public void TogglePopulationTrendFilterItem(GameObject item) {
        if (!item)
            return;
        var toggle = item.GetComponent<Toggle>();
        if (!toggle)
            return;
        //toggle.isOn = !toggle.isOn;
        toggle.Select();
        if (AppLoader.Instance.FilterPopulationTrend != null) {
            if (AppLoader.Instance.FilterPopulationTrend.ContainsKey(toggle.name)) {
                AppLoader.Instance.FilterPopulationTrend[toggle.name] = toggle.isOn;
            }
        }
        AppLoader.Instance.UpdateSpeciesCategoryFilterDataCategory(false);
        ApplyFilter();
    }

    public void ToggleFilterAll(ToggleAllItem item) {
        if (!item || !item.Toggle || !item.Layout)
            return;
        item.Toggle.Select();
        bool selectAll = item.Toggle.isOn;
        foreach (Transform child in item.Layout.transform) {
            var childToggle = child.GetComponent<Toggle>();
            if ((childToggle.isOn && !selectAll) || (!childToggle.isOn && selectAll)) {
                childToggle.isOn = selectAll;
                //childToggle.Select();
            }
        }
        AppLoader.Instance.UpdateSpeciesCategoryFilterDataCategory(false);
        ApplyFilter();
        //if (VideoLoader.Instance.FilterSpecies != null) {
        //    //Trace.Log(JsonConvert.SerializeObject(VideoLoader.Instance.FilterSpecies));
        //}
    }

    public void TogglePrimarySpeciesFilterAll(ToggleAllItem item) {
        if (!item || !item.Toggle || !item.Layout)
            return;
        item.Toggle.Select();
        bool selectAll = item.Toggle.isOn;
        foreach (Transform child in item.Layout.transform) {
            var childToggle = child.GetComponent<Toggle>();
            if ((childToggle.isOn && !selectAll) || (!childToggle.isOn && selectAll)) {
                childToggle.isOn = selectAll;
                //childToggle.Select();
            }
            if (AppLoader.Instance.FilterPrimarySpecies != null) {
                if (AppLoader.Instance.FilterPrimarySpecies.ContainsKey(childToggle.name)) {
                    AppLoader.Instance.FilterPrimarySpecies[childToggle.name] = selectAll;
                }
            }
            if (AppLoader.Instance.CurrentPrimarySpecies != null) { 
                if (AppLoader.Instance.CurrentPrimarySpecies.ContainsKey(childToggle.name)) {
                    AppLoader.Instance.CurrentPrimarySpecies[childToggle.name] = selectAll;
                }
            }
        }
        AppLoader.Instance.UpdateSpeciesCategoryFilterDataCategory(false, !selectAll);
        ApplyFilter();
        //if (VideoLoader.Instance.FilterSpecies != null) {
        //    //Trace.Log(JsonConvert.SerializeObject(VideoLoader.Instance.FilterSpecies));
        //}
    }

    public void ApplyFilter() {
        if (!AppLoader.Instance.m_FilterPanel)
            return;
        ApplyFilter(AppLoader.Instance.m_FilterPanel);
    }

    public void ApplyFilter(GameObject filterPanel) {
        var categoryGrid = filterPanel.FindObject("SpeciesCategoryFilterContent").GetComponent<GridLayoutGroup>();
        var speciesGrid = filterPanel.FindObject("PrimarySpeciesFilterContent").GetComponent<GridLayoutGroup>();
        var iucnGrid = filterPanel.FindObject("IUCNFilterContent").GetComponent<GridLayoutGroup>();
        var populationGrid = filterPanel.FindObject("PopulationTrendFilterContent").GetComponent<GridLayoutGroup>();
        if (AppLoader.Instance.FilterPrimarySpecies != null) {
            foreach (Transform child in speciesGrid.transform) {
                var childToggle = child.GetComponent<Toggle>();
                if (AppLoader.Instance.FilterPrimarySpecies.ContainsKey(childToggle.name)) {
                    if (childToggle.isOn) {
                        AppLoader.Instance.FilterPrimarySpecies[childToggle.name] = true;
                    } else {
                        AppLoader.Instance.FilterPrimarySpecies[childToggle.name] = null;
                    }
                }
            }
            Trace.Log(JsonConvert.SerializeObject(AppLoader.Instance.FilterPrimarySpecies));
            //// Check for "all unticked"
            //if (!VideoLoader.Instance.FilterPrimarySpecies.Any(_ => _.Value.HasValue && _.Value.Value == false)) {
            //    Trace.Log("Clear all species");
            //    VideoLoader.Instance.FilterPrimarySpecies.Clear();
            //}
        }
        if (AppLoader.Instance.FilterIUCN != null) {
            foreach (Transform child in iucnGrid.transform) {
                var childToggle = child.GetComponent<Toggle>();
                if (AppLoader.Instance.FilterIUCN.ContainsKey(childToggle.name)) {
                    if (childToggle.isOn) {
                        AppLoader.Instance.FilterIUCN[childToggle.name] = true;
                    } else {
                        AppLoader.Instance.FilterIUCN[childToggle.name] = null;
                    }
                }
            }
            Trace.Log(JsonConvert.SerializeObject(AppLoader.Instance.FilterIUCN));
        }
        if (AppLoader.Instance.FilterPopulationTrend != null) {
            foreach (Transform child in populationGrid.transform) {
                var childToggle = child.GetComponent<Toggle>();
                if (AppLoader.Instance.FilterPopulationTrend.ContainsKey(childToggle.name)) {
                    if (childToggle.isOn) {
                        AppLoader.Instance.FilterPopulationTrend[childToggle.name] = true;
                    } else {
                        AppLoader.Instance.FilterPopulationTrend[childToggle.name] = null;
                    }
                }
            }
            Trace.Log(JsonConvert.SerializeObject(AppLoader.Instance.FilterPopulationTrend));
        }
        AppLoader.Instance.SetFilterOn();
        AppLoader.Instance.LoadVideos();
        //CloseUI(GameObject.Find("/FilterUI"));
    }

    public void ClearFilter() {
        try {
            Trace.Log("Clearing filter");
            AppLoader.Instance.LoadFilterData();
            AppLoader.Instance.UpdateSpeciesCategoryFilterDataCategory(true);
            ApplyFilter();
            m_SpeciesCategoryToggleAll.GetComponent<Toggle>().isOn = true;
            m_SpeciesPrimaryToggleAll.GetComponent<Toggle>().isOn = true;
        } catch (Exception e) {
            Trace.LogError(e.ToString());
        }
    }

    public void LoadVideos(string name) {

        if (AppLoader.Instance) {
            AppLoader.Instance.LoadVideos(name);
        }
    }

    public void ShowVideoInfo() {
        if (VideoPlaying) {
            var player = GameObject.Find("/Video/VideoPlayer/Player").GetComponent<MediaPlayer>();
            if (player.Control.IsPlaying()) {
                videoPausedByInfo = true;
                PlayPauseVideo(true);
            }
        }
        //Trace.Log("Showing Video Info: " + CurrentVideoItem);
        ShowVideoInfo(CurrentVideoItem);
    }

    public void SortVideos(GameObject item) {
        var sort = GameObject.Find("/UI/MainCanvas/Window/Buttons/ViewButtons").GetComponent<VideoSortButtons>();
        var button = item.GetComponent<Button>();
        //button.Select();
        switch (item.name) {
            case "SortFavoritesButton":
                Trace.Log("Reselected favorites");
                if (AppLoader.Instance.Order == AppLoader.SortOrder.Favorites) {
                    item = GameObject.Find("/UI/MainCanvas/Window/Buttons/ViewButtons/SortMostViewedButton");
                }
                break;
            case "SortDownloadsButton":
                Trace.Log("Reselected downloads");
                if (AppLoader.Instance.Order == AppLoader.SortOrder.Downloads) {
                    item = GameObject.Find("/UI/MainCanvas/Window/Buttons/ViewButtons/SortMostViewedButton");
                }
                break;
        }
        sort.SetButtonActive(item);
        AppLoader.Instance.LoadVideos();
    }

    public void ShowVideoInfo(VideoItemView video) {
        //Trace.Log("Video: " + video);
        if (!VideoInfo || !video) {
            return;
        }
        var thumbnail = VideoInfo.FindObject("Thumbnail").GetComponent<Image>();
        var title = VideoInfo.FindObject("Title").GetComponent<TMP_Text>();
        var metadata = VideoInfo.FindObject("Metadata").GetComponent<TMP_Text>();
        var text = VideoInfo.FindObject("Scroll View").FindObject("Viewport").FindObject("Content").GetComponent<TMP_Text>();
        //var scrollBar = VideoInfo.FindObject("Scroll View").FindObject("Scrollbar Vertical");
        //var scrollUp = VideoInfo.FindObject("Scroll View").FindObject("UpButton");
        //var scrollDown = VideoInfo.FindObject("Scroll View").FindObject("DownButton");
        thumbnail.sprite = null;
        if (!text)
            return;
        VideoInfo.FindObject("PlayButton").SetActive(!VideoPlaying);
        var favoriteButton = VideoInfo.FindObject("FavoriteButton");
        var favoriteImage = favoriteButton.FindObject("Image").GetComponent<Image>();
        favoriteImage.sprite = video.Video.Data.IsFavorite ? Resources.Load<Sprite>("ic_gallery_favorite_on") : Resources.Load<Sprite>("ic_gallery_favorite_off");
        var downloadButton = VideoInfo.FindObject("DownloadButton");
        var downloadImage = downloadButton.FindObject("Image").GetComponent<Image>();
        downloadItemInfo = null;
        CurrentVideoItem = video;
        //Events.CheckVideoDownload(video, VideoInfo);
        List<string> metaDataItems = new List<string>();
        string metaData = "", description = video.Video.Description;
        if (video.Video.Data != null) {
            if (video.Video.Data.Metadata != null && video.Video.Data.Metadata.Count > 0) {
                var attribute = video.Video.Data.Metadata.FirstOrDefault(_ => _.Name.Contains("Primary Species"));
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value)) {
                    var subAttribute = video.Video.Data.Metadata.FirstOrDefault(_ => _.Name.Contains("Colloquial Species Name"));
                    if (subAttribute != null) {
                        metaDataItems.Add(string.Format("<b>Primary Species:</b> {0} ({1})", subAttribute.Value, attribute.Value));
                    } else {
                        metaDataItems.Add(string.Format("<b>Primary Species:</b> {0}", attribute.Value));
                    }
                }
                attribute = video.Video.Data.Metadata.FirstOrDefault(_ => _.Name.Contains("Scientific Name"));
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value)) {
                    metaDataItems.Add(string.Format("<b>Scientific Name:</b> <i>{0}</i>", attribute.Value));
                }
                attribute = video.Video.Data.Metadata.FirstOrDefault(_ => _.Name.Contains("IUCN"));
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value)) {
                    metaDataItems.Add(string.Format("<b>IUCN:</b> {0}", attribute.Value));
                }
                attribute = video.Video.Data.Metadata.FirstOrDefault(_ => _.Name.Contains("Population Trend"));
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value)) {
                    metaDataItems.Add(string.Format("<b>Population Trend:</b> {0}", attribute.Value));
                }
                attribute = video.Video.Data.Metadata.FirstOrDefault(_ => _.Name.Contains("Country Obtained"));
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value)) {
                    metaDataItems.Add(string.Format("<b>Country Obtained:</b> {0}", attribute.Value));
                }
                attribute = video.Video.Data.Metadata.FirstOrDefault(_ => _.Name.Contains("Graphic"));
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value)) {
                    metaDataItems.Add(string.Format("<b>Mature Content:</b> {0}", attribute.Value));
                }
                attribute = video.Video.Data.Metadata.FirstOrDefault(_ => _.Name.Contains("Secondary Species"));
                if (attribute != null && !string.IsNullOrEmpty(attribute.Value)) {
                    if (attribute.Value.Length > 50) {
                        description += string.Format("\n\n<b>Secondary Species:</b> {0}", attribute.Value);
                    } else {
                        metaDataItems.Add(string.Format("<b>Secondary Species:</b> {0}", attribute.Value));
                    }
                }
            }
            if (metaDataItems.Count > 0) {
                metaData = string.Join("\r\n", metaDataItems);
                //metaData += "<br>";
            }
        }
        title.text = video.Video.Title;
        metadata.text = metaData;
        //string info = string.Format(@"<b>{0}</b><br><br>{1}{2}", video.Video.Title, metaData, video.Video.Description);
        //info = info.Replace("<p>", "<br>").Replace("</p>", "").Replace("&nbsp;", " ");
        text.text = description;
        if (!string.IsNullOrEmpty(video.Video.Thumbnail)) {
            //await Main.Graphics.DownloadThumbnailAsync(video.Video, (sprite) => thumbnail.sprite = sprite);
            StartCoroutine(LoadThumbnail(thumbnail, video));
        }
        //scrollUp.SetActive(scrollBar.activeSelf);
        //scrollDown.SetActive(scrollBar.activeSelf);
        VideoInfo.SetActive(true);

        if (downloadBusy) {
            if (activeDownload.Video.Data.ID != video.Video.Data.ID) {
                DownloadVideoInfoComplete(video, downloadButton);
                downloadImage.sprite = Resources.Load<Sprite>("ic_gallery_download_blocked");
                downloadButton.gameObject.GetComponent<VRInteractiveItem>().IsGazeable = false;
            } else {
                downloadItemInfo = downloadButton;
                DownloadVideoInfoStart(downloadItemInfo);
                downloadButton.gameObject.GetComponent<VRInteractiveItem>().IsGazeable = true;
            }
        } else {
            DownloadVideoInfoComplete(video, downloadButton);
        }
    }

    public void ShowConfirmVideoDelete(VideoItemView item) {
        if (!m_ConfirmUI)
            return;
        m_ConfirmUI.SetActive(true);
        DeleteVideo = item;
    }

    public void HideConfirm() {
        if (!m_ConfirmUI)
            return;
        m_ConfirmUI.SetActive(false);
        DeleteVideo = null;
    }

    public void ConfirmDelete() {
        if (DeleteVideo == null)
            return;
        try {
            var path = string.Format("{0}/downloads/{1}", Application.persistentDataPath, DeleteVideo.Video.Data.ID);
            Debug.Log("Delete Path: " + path);
            if (File.Exists(path)) {
                if (downloadBusy) {
                    DownloadRequestCancel = true;
                }
                try {
                    File.Delete(path);
                } catch { }
                Analytics.TrackEvent(string.Format("{0}_{1}", Events.EventVideoDelete, DeleteVideo.Video.Data.Name), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
            }
        } catch (Exception e) {
            Trace.LogError(e.ToString());
            Crashes.TrackError(new Exception(string.Format("Failed to delete video: {0}", e.Message)), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
        }
        AppLoader.Instance.UpdateDownloads();
        AppLoader.Instance.UpdateVideosDownloadStatus();
        HideConfirm();
    }

    IEnumerator LoadThumbnail(Image image, VideoItemView video) {
        yield return Main.Graphics.FromCache(video, true, (sprite) => {
            image.sprite = sprite.Sprite;
        });
        //image.sprite = Main.Graphics.FromCache(url);
        //if (image.sprite == null) {
        //    yield return VideoLoader.Instance.BlobService.GetImageBlob((data) => {
        //        if (data.IsError) {
        //            Trace.LogError(data.ErrorMessage);
        //        } else {
        //            Main.Graphics.SpriteFromTexture2D(data.Data, (sprite) => {
        //                image.sprite = sprite;
        //                Main.Graphics.ToCache(url, image.sprite);
        //            });
        //        }
        //    }, url);
        //}
    }

    //IEnumerator DownloadThumbnail(Image image, string url) {
    //    using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(url)) {
    //        yield return request.SendWebRequest();
    //        if (!request.isNetworkError) {
    //            var texture = ((DownloadHandlerTexture)request.downloadHandler).texture as Texture2D;
    //            Texture2D newTexture = new Texture2D(texture.width, texture.height, texture.format, true);
    //            newTexture.anisoLevel = 4;
    //            newTexture.mipMapBias = -0.7f;
    //            newTexture.filterMode = FilterMode.Trilinear;
    //            newTexture.requestedMipmapLevel = 4;
    //            newTexture.LoadImage(request.downloadHandler.data);
    //            texture = null;
    //            image.sprite = SpriteFromTexture2D(newTexture);
    //        }
    //    }
    //}

    Sprite SpriteFromTexture2D(Texture2D texture) {
        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }

    public void ToggleFavoriteVideo(GameObject button) {
        if (!CurrentVideoItem)
            return;
        ToggleFavoriteVideo(CurrentVideoItem);
        var image = button.gameObject.FindObject("Image").GetComponent<Image>();
        image.sprite = CurrentVideoItem.Video.Data.IsFavorite ? Resources.Load<Sprite>("ic_gallery_favorite_on") : Resources.Load<Sprite>("ic_gallery_favorite_off");
    }

    public void ToggleFavoriteVideo(VideoItemView video) {
        var item = AppLoader.Instance.Data.Videos.FirstOrDefault(_ => _.ID == video.Video.Data.ID);
        if (item == null)
            return;
        item.IsFavorite = !item.IsFavorite;
        var favoriteImage = video.Favorite.gameObject.FindObject("Image").GetComponent<Image>();
        favoriteImage.sprite = video.Video.Data.IsFavorite ? Resources.Load<Sprite>("ic_gallery_favorite_on") : Resources.Load<Sprite>("ic_gallery_favorite_off");
        AppLoader.Instance.SetFavorites();
        if (item.IsFavorite) {
            Analytics.TrackEvent(string.Format("{0}_{1}", Events.EventVideoFavorite, video.Video.Data.Name), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
        } else {
            Analytics.TrackEvent(string.Format("{0}_{1}", Events.EventVideoUnfavorite, video.Video.Data.Name), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
        }
    }

    public static bool downloadBusy = false;
    public bool DownloadRequestCancel = false;
    public static VideoItemView activeDownload;
    static GameObject downloadItemList, downloadItemInfo;

    public void DownloadVideo(GameObject item) {
        if (!CurrentVideoItem)
            return;
        downloadItemInfo = item;
        if (CurrentVideoItem.Video.Data.IsDownloaded) {
            ShowConfirmVideoDelete(CurrentVideoItem);
        } else {
            DownloadVideo(CurrentVideoItem, null);
        }
    }

    public event Action<bool> VideoDownloadComplete;

    public async void DownloadVideo(VideoItemView video, Action<bool> listener) {
        var path = string.Format("{0}/downloads/{1}", Application.persistentDataPath, video.Video.Data.ID);
        if (video.Video.Data.IsDownloaded) {
            ShowConfirmVideoDelete(video);
            return;
        }
        if (downloadBusy) {
            ShowConfirmVideoDelete(video);
            return;
        }
        if (!Directory.Exists(Path.GetDirectoryName(path))) {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
        }
        if (video.Download != null) {
            downloadItemList = video.Download.gameObject;
            if (downloadItemList != null && downloadItemList.activeSelf) {
                DownloadVideoInfoStart(downloadItemList);
            }
        }
        if (downloadItemInfo != null && downloadItemInfo.activeSelf) {
            DownloadVideoInfoStart(downloadItemInfo);
        }
        activeDownload = video;
        downloadBusy = true;
        AppLoader.Instance.UpdateVideosDownloadStatus();
        Trace.Log("Downloading to: " + path);
        string url = await AppLoader.Instance.GetVideoDownloadUrl(video.Video.Data);
        Trace.Log("Download URL: " + url);
        try {
            StartCoroutine(DownloadVideoAsync(video, path, url, listener));
        } catch (Exception e) {
            Trace.LogError(e.ToString());
            Crashes.TrackError(new Exception(string.Format("Failed to download video. Please check your internet connection and retry.\r\n\r\n({0})", e.Message)), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
            activeDownload = null;
            downloadBusy = false;
        }
    }

    private static void DownloadVideoInfoStart(GameObject item) {
        if (!item || item == null || !item.activeInHierarchy)
            return;
        item.FindObject("Image").SetActive(false);
        item.FindObject("Text").GetComponent<TMP_Text>().text = "0%";
        item.FindObject("Text").SetActive(true);
        item.FindObject("Selection").SetActive(true);
    }

    private static void DownloadVideoInfoProgress(GameObject item, float progress) {
        if (!item || item == null || !item.activeInHierarchy)
            return;
        try {
            item.FindObject("Image").SetActive(false);
            item.FindObject("Text").GetComponent<TMP_Text>().text = string.Format("{0}%", (int)(progress * 100f));
            item.FindObject("Selection").GetComponent<Image>().fillAmount = progress;
        } catch { }
    }

    private static void DownloadVideoInfoComplete(VideoItemView video, GameObject item) {
        if (!item || item == null || !item.activeInHierarchy)
            return;
        try {
            item.FindObject("Image").SetActive(true);
            item.FindObject("Image").GetComponent<Image>().sprite = video.Video.Data.IsDownloaded ? Resources.Load<Sprite>("ic_gallery_download_on") : Resources.Load<Sprite>("ic_gallery_download_off");
            item.FindObject("Text").SetActive(false);
            item.FindObject("Selection").SetActive(false);
        } catch { }
    }

    public static void CheckVideoDownload(VideoItemView video, GameObject item) {
        try {
            if (!downloadBusy)
                return;
            Trace.Log("CheckVideoDownload: " + activeDownload.Video.Data.ID + " = " + video.Video.Data.ID);
            if (video.Video.Data.ID == activeDownload.Video.Data.ID) {
                Trace.Log("CheckVideoDownload: Found active");
                downloadItemList = item;
                //DownloadVideoInfoStart(downloadItemList);
            }
        } catch { }
    }

    public IEnumerator DownloadVideoAsync(VideoItemView video, string path, string url, Action<bool> listener) {
        try {
            string xmlUrl = string.Format("{0}&comp=list&restype=container", url);
            //Trace.Log("XML URL: " + xmlUrl);
            using (UnityWebRequest webRequest = UnityWebRequest.Get(xmlUrl)) {
                //webRequest.SetRequestHeader("Authorization", "Bearer " + VideoLoader.Instance.AccessToken);
                webRequest.timeout = 15;
                yield return webRequest.SendWebRequest();
                if (webRequest.isNetworkError || webRequest.isHttpError) {
                    string error = string.Format("Download Request Failed:\n\n{0}\n\nPlease try again.", webRequest.error);
                    //Trace.LogError(error);
                    activeDownload = null;
                    downloadBusy = false;
                    ShowError(error);
                    AppLoader.Instance.UpdateVideosDownloadStatus();
                    //throw new Exception(webRequest.error);
                } else {
                    string xml = webRequest.downloadHandler.text;
                    if (!string.IsNullOrEmpty(xml)) {
                        string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                        if (xml.StartsWith(_byteOrderMarkUtf8)) {
                            xml = xml.Remove(0, _byteOrderMarkUtf8.Length);
                        }
                        var details = GetDownloadVideoDetails(xml);
                        Trace.Log(JsonConvert.SerializeObject(details));
                        if (details != null && details.Valid) {
                            string downloadUrl = url.Replace("?", string.Format("/{0}?", details.Name));
                            downloadUrl = downloadUrl.Replace(" ", "%20");
                            Trace.Log("New Download URL: " + downloadUrl);
                            using (UnityWebRequest downloadRequest = UnityWebRequest.Get(downloadUrl)) {
                                var handler = new DownloadHandlerFile(path, false);
                                handler.removeFileOnAbort = true;
                                downloadRequest.downloadHandler = handler;
                                //downloadRequest.SetRequestHeader("Authorization", "Bearer " + VideoLoader.Instance.AccessToken);
                                downloadRequest.SendWebRequest();
                                int lastProgress = 0;
                                try {
                                    while (!downloadRequest.isDone) {
                                        int progress = (int)(downloadRequest.downloadProgress * 100f);
                                        if (lastProgress != progress) {
                                            //Trace.Log("Downloaded " + progress.ToString() + "%");
                                            if (downloadItemList && downloadItemList != null && downloadItemList.activeSelf) {
                                                DownloadVideoInfoProgress(downloadItemList, downloadRequest.downloadProgress);
                                            }
                                            if (downloadItemInfo && downloadItemInfo != null && downloadItemInfo.activeSelf) {
                                                DownloadVideoInfoProgress(downloadItemInfo, downloadRequest.downloadProgress);
                                            }
                                            lastProgress = progress;
                                        }
                                        if (!downloadBusy) {
                                            break;
                                        }
                                        if (DownloadRequestCancel) {
                                            break;
                                        }
                                        yield return null;
                                    }
                                    activeDownload = null;
                                    downloadBusy = false;
                                    if (downloadRequest.isNetworkError || downloadRequest.isHttpError) {
                                        string error = "";
                                        try {
                                            error = string.Format("Download Request Failed: {0}", downloadRequest.error);
                                            error += string.Format(" ({0})", downloadRequest.downloadHandler.text);
                                        } catch { }
                                        Trace.LogError(error);
                                        ShowError(error);
                                        activeDownload = null;
                                        downloadBusy = false;
                                        //throw new Exception(downloadRequest.error);
                                    } else {
                                        Trace.Log("Download done?");
                                        FileInfo fi = new FileInfo(path);
                                        if (fi.Exists) {
                                            if (fi.Length == details.Size) {
                                                video.Video.Data.IsDownloaded = true;
                                                Trace.Log("File downloaded successfully: " + fi.Length);
                                                Analytics.TrackEvent(string.Format("{0}_{1}", Events.EventVideoDownload, video.Video.Data.Name), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
                                                if (listener != null) {
                                                    listener.Invoke(true);
                                                }
                                            } else {
                                                video.Video.Data.IsDownloaded = false;
                                                try {
                                                    fi.Delete();
                                                } catch { }
                                                if (!DownloadRequestCancel) {
                                                    string error = "Download did not complete successfully. Please try again.";
                                                    Trace.LogError(error);
                                                    ShowError(error);
                                                }
                                                activeDownload = null;
                                                downloadBusy = false;
                                            }
                                        } else {
                                            // Error??
                                            string error = "Download did not complete successfully. Please try again.";
                                            Trace.LogError(error);
                                            ShowError(error);
                                            activeDownload = null;
                                            downloadBusy = false;
                                        }
                                        if (downloadItemList != null && downloadItemList.activeSelf) {
                                            DownloadVideoInfoComplete(video, downloadItemList);
                                        }
                                        if (downloadItemInfo != null && downloadItemInfo.activeSelf) {
                                            DownloadVideoInfoComplete(video, downloadItemInfo);
                                        }
                                        Trace.Log("Closing active download: " + activeDownload);
                                    }
                                } finally {
                                    AppLoader.Instance.UpdateVideosDownloadStatus();
                                    DownloadRequestCancel = false;
                                }
                            }
                        }
                    }
                }
            }
        } finally {

        }
    }

    private DownloadVideoDetails GetDownloadVideoDetails(string xml) {
        DownloadVideoDetails result = new DownloadVideoDetails();
        //Trace.Log(xml);
        XmlDocument document = new XmlDocument();
        document.LoadXml(xml);
        XmlNodeList nodes = document.GetElementsByTagName("Blob");
        if (nodes != null && nodes.Count > 0) {
            foreach (XmlNode node in nodes) {
                if (node.HasChildNodes && node.ChildNodes.Count == 2) {
                    XmlNode nameNode = node.ChildNodes[0];
                    XmlNode propertiesNode = node.ChildNodes[1];
                    if (nameNode.InnerText.Contains("4096x2048") && nameNode.InnerText.Contains(".mp4")) {
                        result.Name = nameNode.InnerText;
                        foreach (XmlNode propertyNode in propertiesNode.ChildNodes) {
                            if (propertyNode.Name.Equals("Content-Length")) {
                                long size = 0;
                                if (long.TryParse(propertyNode.InnerText, out size)) {
                                    result.Size = size;
                                }
                            }
                        }
                    }
                }
            }
        }
        result.Valid = !string.IsNullOrEmpty(result.Name) && result.Size > 0;
        return result;
    }

    public class DownloadVideoDetails {
        public string Name { get; set; }
        public long Size { get; set; }
        public bool Valid { get; set; }
    }

    public void HideVideoInfo() {
        if (!VideoInfo)
            return;
        VideoInfo.SetActive(false);
        if (VideoPlaying && videoPausedByInfo) {
            PlayPauseVideo();
        }
        videoPausedByInfo = false;
    }

    public void PlayVideo() {
        if (CurrentVideoItem == null)
            return;
        PlayVideo(CurrentVideoItem);
    }

    public async void PlayVideo(VideoItemView video) {
        if (!video)
            return;
        //var video = item.GetComponent<VideoItemView>();
        //if (CurrentVideoItem != null) {
        //    video = CurrentVideoItem;
        //} else {
        //    CurrentVideoItem = video;
        //}
        CurrentVideoItem = video;
        if (VideoInfo) {
            VideoInfo.SetActive(false);
        }
        int startTime = 0;
        if (video.Video.Data != null) {
            if (video.Video.Data.Metadata != null && video.Video.Data.Metadata.Count > 0) {
                var attribute = video.Video.Data.Metadata.FirstOrDefault(_ => _.Name.Contains("Start Time"));
                if (attribute != null) {
                    string[] parts = attribute.Value.Split(new string[] { "." }, StringSplitOptions.None);
                    if (parts != null && parts.Length == 4) {
                        startTime = ((int.Parse(parts[1]) * 60) + int.Parse(parts[2]) * 1000);
                        //Trace.Log("Start Time: " + attribute.Value + " = " + startTime);
                    }
                }
            }
        }
        ////Trace.Log(video.Video.Url);
        //CurrentVideo = video.Video.Url;
        //SceneManager.LoadScene("VideoScene");
        var audio = GameObject.Find("/OVRCameraRig").GetComponent<AudioSource>();
        audio.Pause();
        GameObject.Find("/World").FindObject("Globe").SetActive(false);
        m_UI.SetActive(false);
        if (m_AllMarker) {
            m_AllMarker.SetActive(false);
        }
        if (m_SpecialMarker) {
            m_SpecialMarker.SetActive(false);
        }
        m_FilterUI.SetActive(false);
        //GameObject.Find("/Video").FindObject("UI").SetActive(true);
        m_Loading.SetActive(true);
        GameObject.Find("/LoadingUI/MainCanvas/Loading/Text").GetComponent<TMP_Text>().text = "Loading";
        GameObject.Find("/Video").FindObject("VideoPlayer").SetActive(true);
        var videoMenu = GameObject.Find("/Video").FindObject("VideoControlUI");
        videoMenu.SetActive(true);
        var menuButton = GameObject.Find("/Video/VideoControlUI/MenuButton").GetComponent<Image>();
        menuButton.sprite = Resources.Load<Sprite>("ic_menu");
        var videoSubMenu = GameObject.Find("/Video/VideoControlUI/Menu");
        GameObject.Find("/Video").FindObject("MenuUI").SetActive(false);
        mediaPlayer = GameObject.Find("/Video/VideoPlayer/Player").GetComponent<MediaPlayer>();
        var playButton = GameObject.Find("/Video/VideoControlUI/Menu/PlayButton").GetComponent<Image>();
        playButton.sprite = Resources.Load<Sprite>("ic_pause");
        mediaProgress = GameObject.Find("/Video/VideoControlUI/Menu/PlayButton/Selection").GetComponent<Image>();
        mediaProgress.fillAmount = 0;
        mediaSeekSlider = GameObject.Find("/Video/VideoControlUI/Menu/Seek/Slider").GetComponent<Slider>();
        mediaSeekCurrent = GameObject.Find("/Video/VideoControlUI/Menu/Seek/CurrentTime").GetComponent<TMP_Text>();
        mediaSeekEnd = GameObject.Find("/Video/VideoControlUI/Menu/Seek/EndTime").GetComponent<TMP_Text>();
        videoSubMenu.SetActive(false);
        var quality = GameObject.Find("/Video/VideoControlUI/Menu/Seek/Quality/Text").GetComponent<TMP_Text>();
        //Trace.Log("Play URL: " + CurrentVideo);
        VideoPlaying = true;
        mediaPlayer.Events.AddListener((p, e, s) => {
            Trace.Log("Video Event: " + e.ToString());
            if (e == MediaPlayerEvent.EventType.StartedBuffering) {
                //GameObject.Find("/Video").FindObject("UI").SetActive(true);
            } else if (e == MediaPlayerEvent.EventType.Started) {
                //GameObject.Find("/Video").FindObject("UI").SetActive(false);
                m_Loading.SetActive(false);
                //GameObject.Find("/Video").FindObject("VideoControlUI").SetActive(true);
            } else if (e == MediaPlayerEvent.EventType.FirstFrameReady) {
                if (startTime > 0) {
                    p.Control.SeekFast(startTime);
                }
            } else if (e == MediaPlayerEvent.EventType.FinishedPlaying) {
                Trace.Log("Closing video: " + CurrentVideo);
                //p.Control.CloseVideo();
                CloseVideo();
            } else if (e == MediaPlayerEvent.EventType.ResolutionChanged) {
                quality.text = EstimateVideoQuality(p.Info);
            }
            if (p.Control.IsPaused()) {
                playButton.sprite = Resources.Load<Sprite>("ic_play");
            } else if (p.Control.IsPlaying()) {
                playButton.sprite = Resources.Load<Sprite>("ic_pause");
            }
        });
        //
        try {
            if (video.Video.Data.IsDownloaded) {
                CurrentVideo = string.Format("{0}/downloads/{1}", Application.persistentDataPath, video.Video.Data.ID);
                mediaPlayer.ForceFileFormat = FileFormat.Unknown;
                Trace.Log("Local File: " + CurrentVideo);
            } else {
                CurrentVideo = await AppLoader.Instance.GetVideoUrl(video.Video.Data);
                mediaPlayer.ForceFileFormat = FileFormat.DASH;
                Trace.Log("Stream URL: " + CurrentVideo);
            }
        } catch (Exception e) {
            Trace.LogError(e.ToString());
            Crashes.TrackError(new Exception(string.Format("Failed to load video: {0}", e.Message)), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
            return;
        }
        if (string.IsNullOrEmpty(CurrentVideo)) {
            GameObject.Find("/LoadingUI/MainCanvas/Loading/Text").GetComponent<TMP_Text>().text = "Error: Could not load the video";
            Crashes.TrackError(new Exception("Failed to load video"), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
        } else {
            //player.PlatformOptionsAndroid.httpHeaderJson = string.Format("{{\"Authorization\", \"{0}\"}}", VideoLoader.Instance.AccessToken);
            mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, CurrentVideo, true);
            Analytics.TrackEvent(string.Format("{0}_{1}", Events.EventVideoPlay, video.Video.Data.Name), new Events.EventTraceProperties().Add(GetVideoStreamProperties(mediaPlayer)).Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
        }
        Trace.Log("Playing: " + CurrentVideo);
        //player.SetActive(true);
        //RenderSettings.skybox = VideoSkybox;
        //GameObject.Find("/Video").FindObject("Menu").SetActive(true);
        //var videoPlayer = player.GetComponent<MoviePlayer>();
        //videoPlayer.URL = CurrentVideo;
        //videoPlayer.Play(CurrentVideo, "");
        //StartCoroutine(PlayVideoUrl(videoPlayer, CurrentVideo));
        //await player.GetComponent<YoutubePlayer.YoutubePlayer>().PlayVideoAsync(CurrentVideo);
    }

    bool safariVideoDownloading = false;
    public async void PlayTestSafariVideo(GameDrive drive, VideoDataItem video) {
        if (video == null)
            return;
        var loading = GameObject.Find("/LoadingUI");
        var downloadLoading = GameObject.Find("/TempDownloadUI");
        if (loading && loading.activeSelf) {
            loading.SetActive(false);
        }
        Trace.Log("Safari video?: " + video.IsDownloaded);
        if (!video.IsDownloaded) {
            if (safariVideoDownloading)
                return;
            Trace.Log("Downloading safari video");
            DownloadVideo(new VideoItemView { Video = new VideoItem { Data = video } }, new Action<bool>((success) => {
                Trace.Log("Safari download completed?: " + video.IsDownloaded);
                PlayTestSafariVideo(drive, video);
                return;
            }));
            safariVideoDownloading = true;
            return;
        }
        Trace.Log("Download safari video downloaded?");
        if (downloadLoading && downloadLoading.activeSelf) {
            downloadLoading.SetActive(false);
        }
        if (VideoInfo) {
            VideoInfo.SetActive(false);
        }
        var audio = GameObject.Find("/OVRCameraRig").GetComponent<AudioSource>();
        audio.Pause();
        m_UI.SetActive(false);
        if (m_AllMarker) {
            m_AllMarker.SetActive(false);
        }
        if (m_SpecialMarker) {
            m_SpecialMarker.SetActive(false);
        }
        m_FilterUI.SetActive(false);
        m_Loading.SetActive(true);
        m_GameDriveUI.SetActive(true);
        GameObject.Find("/LoadingUI/MainCanvas/Loading/Text").GetComponent<TMP_Text>().text = "Loading";
        GameObject.Find("/Video").FindObject("VideoPlayer").SetActive(true);
        GameDriveCanvas = GameObject.Find("/Video/VideoPlayer/GameDriveUI/MainCanvas");
        GameDriveSphere = GameObject.Find("/Video/VideoPlayer/GameDriveUI/MainCanvas").FindObject("Template");
        vRRawRaycaster = GameObject.Find("/EventSystem").GetComponent<OVRRawRaycaster>();
        var videoMenu = GameObject.Find("/Video").FindObject("VideoControlUI");
        videoMenu.SetActive(true);
        var menuButton = GameObject.Find("/Video/VideoControlUI/MenuButton").GetComponent<Image>();
        menuButton.sprite = Resources.Load<Sprite>("ic_menu");
        var videoSubMenu = GameObject.Find("/Video/VideoControlUI/Menu");
        GameObject.Find("/Video").FindObject("MenuUI").SetActive(false);
        mediaPlayer = GameObject.Find("/Video/VideoPlayer/Player").GetComponent<MediaPlayer>();
        var playButton = GameObject.Find("/Video/VideoControlUI/Menu/PlayButton").GetComponent<Image>();
        playButton.sprite = Resources.Load<Sprite>("ic_pause");
        mediaProgress = GameObject.Find("/Video/VideoControlUI/Menu/PlayButton/Selection").GetComponent<Image>();
        mediaProgress.fillAmount = 0;
        mediaSeekSlider = GameObject.Find("/Video/VideoControlUI/Menu/Seek/Slider").GetComponent<Slider>();
        mediaSeekCurrent = GameObject.Find("/Video/VideoControlUI/Menu/Seek/CurrentTime").GetComponent<TMP_Text>();
        mediaSeekEnd = GameObject.Find("/Video/VideoControlUI/Menu/Seek/EndTime").GetComponent<TMP_Text>();
        videoSubMenu.SetActive(false);
        var quality = GameObject.Find("/Video/VideoControlUI/Menu/Seek/Quality/Text").GetComponent<TMP_Text>();
        //Trace.Log("Play URL: " + CurrentVideo);
        VideoPlaying = true;
        CurrentGameDrive = drive;
        Trace.Log(JsonConvert.SerializeObject(CurrentGameDrive));
        mediaPlayer.Events.AddListener((p, e, s) => {
            Trace.Log("Video Event: " + e.ToString());
            if (e == MediaPlayerEvent.EventType.StartedBuffering) {
                //GameObject.Find("/Video").FindObject("UI").SetActive(true);
            } else if (e == MediaPlayerEvent.EventType.Started) {
                //GameObject.Find("/Video").FindObject("UI").SetActive(false);
                m_Loading.SetActive(false);
                //GameObject.Find("/Video").FindObject("VideoControlUI").SetActive(true);
            } else if (e == MediaPlayerEvent.EventType.FirstFrameReady) {
                //p.Control.SeekFast(32000);
            } else if (e == MediaPlayerEvent.EventType.FinishedPlaying) {
                Trace.Log("Closing video: " + CurrentVideo);
                CloseVideo();
            } else if (e == MediaPlayerEvent.EventType.ResolutionChanged) {
                quality.text = EstimateVideoQuality(p.Info);
            }

            if (p.Control.IsPaused()) {
                playButton.sprite = Resources.Load<Sprite>("ic_play");
            } else if (p.Control.IsPlaying()) {
                playButton.sprite = Resources.Load<Sprite>("ic_pause");
            }
        });
        //
        try {
            //CurrentVideo = await AppLoader.Instance.GetVideoUrl(video);
            //mediaPlayer.ForceFileFormat = FileFormat.DASH;
            CurrentVideo = string.Format("{0}/downloads/{1}", Application.persistentDataPath, video.ID);
            mediaPlayer.ForceFileFormat = FileFormat.Unknown;
            Trace.Log("File Path: " + CurrentVideo);
        } catch (Exception e) {
            Trace.LogError(e.ToString());
            Crashes.TrackError(new Exception(string.Format("Failed to load video: {0}", e.Message)), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
            return;
        }
        if (string.IsNullOrEmpty(CurrentVideo)) {
            GameObject.Find("/LoadingUI/MainCanvas/Loading/Text").GetComponent<TMP_Text>().text = "Error: Could not load the video";
            Crashes.TrackError(new Exception("Failed to load video"), new Events.EventTraceProperties().Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
        } else {
            //player.PlatformOptionsAndroid.httpHeaderJson = string.Format("{{\"Authorization\", \"{0}\"}}", VideoLoader.Instance.AccessToken);
            mediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, CurrentVideo, true);
            Analytics.TrackEvent(string.Format("{0}_{1}", Events.EventVideoPlay, "2020.12.12.-360-Chris-WildX-Thornybush-Helicopter.mp4"), new Events.EventTraceProperties().Add(GetVideoStreamProperties(mediaPlayer)).Add(Events.GetProperties()).Add(Events.GetOculusProperties()).Properties);
        }
        Trace.Log("Playing: " + CurrentVideo);
    }

    public static Dictionary<string, string> GetVideoStreamProperties(MediaPlayer player) {
        Dictionary<string, string> result = new Dictionary<string, string> {
            { "Path", player.m_VideoPath },
            { "Quality", EstimateVideoQuality(player.Info) }
        };
        return result;
    }

    private static string EstimateVideoQuality(IMediaInfo info) {
        int height = info.GetVideoHeight();
        if (height >= 4000) {
            return "8K";
        } else if (height >= 3000) {
            return "6K";
        } else if (height >= 2700) {
            return "5K";
        } else if (height >= 2000) {
            return "4K";
        } else if (height >= 1000) {
            return "HD";
        } else if (height >= 700) {
            return "720";
        } else {
            return "SD";
        }
    }

    public void Update() {
        try {
            if (mediaPlayer != null && mediaProgress != null && mediaPlayer.Control != null && mediaPlayer.Info != null) {
                //if (mediaPlayer.Control.IsPlaying()) {
                try {
                    float progress = mediaPlayer.Control.GetCurrentTimeMs() / mediaPlayer.Info.GetDurationMs();
                    if (mediaProgress) {
                        mediaProgress.fillAmount = progress;
                    }
                    if (mediaSeekSlider) {
                        mediaSeekSlider.value = progress;
                    }
                    if (mediaSeekCurrent) {
                        mediaSeekCurrent.text = string.Format("{0:mm\\:ss}", TimeSpan.FromMilliseconds(mediaPlayer.Control.GetCurrentTimeMs()));
                    }
                    if (mediaSeekEnd) {
                        mediaSeekEnd.text = string.Format("{0:mm\\:ss}", TimeSpan.FromMilliseconds(mediaPlayer.Info.GetDurationMs()));
                    }
                    if (CurrentGameDrive != null) {
                        try {
                            ProcessGameDrive();
                        } catch { }
                        try {
                            ProcessGameDriveInput();
                        } catch { }
                    }
                } catch (Exception e) {
                    Trace.LogError(e.ToString());
                }
                //}
            }
        } catch (Exception e) {
            Trace.LogError(e.ToString());
        }
    }

    private void ProcessGameDrive() {
        float time = mediaPlayer.Control.GetCurrentTimeMs();
        var items = CurrentGameDrive.Items.Where(_ => time >= _.StartTime.TotalMilliseconds && time <= _.EndTime.TotalMilliseconds).ToList();
        if (items != null && items.Count > 0) {
            foreach (var item in items) {
                if (item.EventType == GameDriveEventType.MotionTracking) {
                    ProcessGameDriveMotionTracking(item);
                }
            }
        }
    }

    private GameDriveDataItem markerBeingCreated = null;
    private GameObject markerSphereBeingCreated = null;
    private Vector3 pointerTransform = Vector3.zero;
    private void ProcessGameDriveInput() {
        if (OVRInput.GetUp(OVRInput.RawButton.B)) {
            if (mediaPlayer.Control.IsPaused()) {
                mediaPlayer.Control.Play();
            } else {
                mediaPlayer.Control.Pause();
            }
        }
        float time = mediaPlayer.Control.GetCurrentTimeMs();
        if (OVRInput.Get(OVRInput.RawButton.RThumbstickLeft)) {
            mediaPlayer.Control.Seek(time - (200 * Math.Abs(OVRInput.Get(OVRInput.RawAxis2D.RThumbstick).x)));
        } else if (OVRInput.Get(OVRInput.RawButton.RThumbstickRight)) {
            mediaPlayer.Control.Seek(time + (200 * Math.Abs(OVRInput.Get(OVRInput.RawAxis2D.RThumbstick).x)));
        }
        if (Input.GetKeyDown(KeyCode.Space)) {
            pointerTransform = GameObject.Find("/OVRCameraRig/TrackingSpace/CenterEyeAnchor/Camera UI").FindObject("Reticle").transform.position;
        }
        if (Input.GetKeyDown(KeyCode.Space) || OVRInput.Get(OVRInput.RawButton.RIndexTrigger)) {
            if (markerBeingCreated == null) {
                Trace.Log("Creating marker");
                if (pointerTransform != Vector3.zero) {
                    Debug.Log("Creating gamedrive item: " + pointerTransform);
                    markerBeingCreated = new GameDriveDataItem {
                        StartPositionX = pointerTransform.x,
                        StartPositionY = pointerTransform.y,
                        StartPositionZ = pointerTransform.z
                    };
                    markerSphereBeingCreated = Instantiate(GameDriveSphere);
                    markerSphereBeingCreated.transform.SetParent(GameDriveCanvas.transform);
                    markerSphereBeingCreated.transform.position = pointerTransform;
                    markerSphereBeingCreated.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                    markerSphereBeingCreated.SetActive(true);
                }
            } else {
                Vector3 scale = markerSphereBeingCreated.transform.localScale;
                scale.x += OVRInput.Get(OVRInput.RawAxis1D.RIndexTrigger) * 0.01f;
                scale.y += OVRInput.Get(OVRInput.RawAxis1D.RIndexTrigger) * 0.01f;
                scale.z += OVRInput.Get(OVRInput.RawAxis1D.RIndexTrigger) * 0.01f;
                markerSphereBeingCreated.transform.localScale = scale;
            }
        } else if (OVRInput.Get(OVRInput.RawButton.RHandTrigger)) {
            if (markerBeingCreated != null) {
                Vector3 scale = markerSphereBeingCreated.transform.localScale;
                if (scale.x > 0.1 && scale.y > 0.1) {
                    scale.x -= OVRInput.Get(OVRInput.RawAxis1D.RHandTrigger) * 0.01f;
                    scale.y -= OVRInput.Get(OVRInput.RawAxis1D.RHandTrigger) * 0.01f;
                    scale.z -= OVRInput.Get(OVRInput.RawAxis1D.RHandTrigger) * 0.01f;
                }
                markerSphereBeingCreated.transform.localScale = scale;
            }
        }
        if (OVRInput.Get(OVRInput.RawButton.A)) {
            CurrentGameDrive.Items.Add(markerBeingCreated);
            Destroy(markerSphereBeingCreated);
            markerSphereBeingCreated = null;
            markerBeingCreated = null;
            pointerTransform = Vector3.zero;
        }
    }

    public void ProcessPointerSelection(RaycastHit hit, GameObject item) {
        pointerTransform = hit.point;
        //ShowError(string.Format("Hit: {0},{1},{2}", pointerTransform.x, pointerTransform.y, pointerTransform.z));
    }

    private void ProcessGameDriveMotionTracking(GameDriveDataItem item) {
        int height = mediaPlayer.Info.GetVideoHeight();
        float rel = height / 2560f;
        float ppm = height / 100f;
        //
        Vector3 coords = new Vector3((item.EndPositionX + item.StartPositionX) / 2, (item.EndPositionY + item.StartPositionY) / 2, (item.EndPositionZ + item.StartPositionZ) / 2);
        if (coords.x == 0)
            coords.x = Mathf.Epsilon;
        var radius = Mathf.Sqrt((coords.x * coords.x)
                        + (coords.y * coords.y)
                        + (coords.z * coords.z));
        var radic = (radius * rel) / ppm;
        var polar = Mathf.Atan(coords.z / coords.x);
        if (coords.x < 0)
            polar += Mathf.PI;
        var elevation = Mathf.Asin(coords.y / radius);
        Vector3 outCoords = new Vector3(polar * ppm, 0 - (elevation * ppm), radius / radic);
        //Vector3 outCoords = new Vector3();
        //float a = coords.z * Mathf.Cos(coords.y);
        //outCoords.x = a * Mathf.Cos(coords.x);
        //outCoords.y = coords.z * Mathf.Sin(coords.y);
        //outCoords.z = a * Mathf.Sin(coords.x);

        //
        //float ppm = 20f;
        //float x = (((item.EndPositionX + item.StartPositionX) / 2) * rel) / ppm;
        //float y = (((item.EndPositionY + item.StartPositionY) / 2) * rel) / ppm;
        //float z = (((item.EndPositionZ + item.StartPositionZ) / 2) * rel) / ppm;
        //GameDriveEllipse.transform.localPosition = outCoords;
    }

    IEnumerator PlayVideoUrl(VideoPlayer player, string url) {
        player.source = VideoSource.Url;
        player.playOnAwake = false;
        player.frame = 0;
        player.url = url;
        //Trace.Log("Preparing video: " + url);
        player.Prepare();
        while (!player.isPrepared) {
            yield return null;
        }
        //Trace.Log("Playing video: " + url);
        player.Play();
    }

    public void CloseVideo() {
        //SceneManager.UnloadSceneAsync("VideoScene");
        //SceneManager.LoadScene("WorldScene");
        //var player = GameObject.Find("/Video").FindObject("MoviePlayer");
        //player.GetComponent<YoutubePlayer.YoutubePlayer>().Cancel();
        //Trace.Log("CloseVideo");
        if (VideoPlaying) {
            var player = GameObject.Find("/Video/VideoPlayer/Player").GetComponent<MediaPlayer>();
            player.Stop();
            player.CloseVideo();
            player.Events.RemoveAllListeners();
            //var videoPlayer = player.GetComponent<MoviePlayer>();
            //videoPlayer.Stop();
            //player.SetActive(false);
            GameObject.Find("/Video").FindObject("VideoPlayer").SetActive(false);
            GameObject.Find("/Video").FindObject("UI").SetActive(false);
            GameObject.Find("/Video").FindObject("VideoControlUI").SetActive(false);
            GameObject.Find("/Video").FindObject("MenuUI").SetActive(true);
            m_Loading.SetActive(false);
            m_UI.SetActive(true);
            if (m_AllMarker) {
                m_AllMarker.SetActive(true);
            }
            if (m_SpecialMarker) {
                m_SpecialMarker.SetActive(true);
            }
            if (IsFilterActive) {
                ShowFilterUI();
            }
            GameObject.Find("/World").FindObject("Globe").SetActive(true);
            //RenderSettings.skybox = DefaultSkybox;
            menuActive = false;
            VideoPlaying = false;
            CurrentVideoItem = null;
            HideVideoInfo();
            var audio = GameObject.Find("/OVRCameraRig").GetComponent<AudioSource>();
            audio.Play();
        } else {
            //Trace.Log("Quit!");
            Application.Quit();
        }
    }

    public void PlayPauseVideo() {
        if (VideoPlaying) {
            var player = GameObject.Find("/Video/VideoPlayer/Player").GetComponent<MediaPlayer>();
            var button = GameObject.Find("/Video/VideoControlUI/Menu/PlayButton").GetComponent<Image>();
            if (player.Control.IsPlaying()) {
                player.Control.Pause();
                button.sprite = Resources.Load<Sprite>("ic_play");
            } else {
                player.Control.Play();
                button.sprite = Resources.Load<Sprite>("ic_pause");
                videoPausedByInfo = false;
            }
        }
    }

    public void PlayPauseVideo(bool pause) {
        if (VideoPlaying) {
            var player = GameObject.Find("/Video/VideoPlayer/Player").GetComponent<MediaPlayer>();
            var button = GameObject.Find("/Video/VideoControlUI/Menu/PlayButton").GetComponent<Image>();
            if (pause) {
                player.Control.Pause();
                button.sprite = Resources.Load<Sprite>("ic_play");
            } else {
                //HideVideoInfo();
                player.Control.Play();
                button.sprite = Resources.Load<Sprite>("ic_pause");
            }
        }
    }

    public void VideoPlayerEvent(MediaPlayer player, EventType type, string errorCode) {

    }

    public void ShowDropDown(TMP_Dropdown dropdown) {
        if (!dropdown)
            return;
        dropdown.Show();
    }

    public void HideDropDown(TMP_Dropdown dropdown) {
        if (!dropdown)
            return;
        dropdown.Hide();
    }

    public void Back() {
        //Trace.Log("Back");
        //if (m_Reticle) {
        //    m_Reticle.SetActive(true);
        //}
        //if (m_Menu) {
        //    m_Menu.SetActive(false);
        //}
        //if (m_Player) {
        //    m_Player.Cancel();
        //}
        //if (m_Globe) {
        //    m_Globe.Show();
        //}
    }
}
