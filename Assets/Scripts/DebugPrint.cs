﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts {
    public class DebugPrint : MonoBehaviour {
        private static DebugPrint instance = null;

        float xTextPrintOffset = 3.0f;
        Vector2 scrollPosition = Vector2.zero;
        //change windowRect for initial position and size.
        Rect windowRect = new Rect(20, 20, 300, 400);
        int windowId = 1;

        //list that contains the info to be printed to the window
        public List<PrintInfo> printList = null;

        //printInfo tells the output to be boxed or labeled
        public class PrintInfo {
            public bool boxBool;
            public string buffer;
            public PrintInfo(string buffer) {
                this.buffer = buffer;
                boxBool = false;
            }

            public PrintInfo(string buffer, bool boxBool) {
                this.buffer = buffer;
                this.boxBool = boxBool;
            }
        }

        //a common singleton class for unity
        public static DebugPrint Instance {
            get {
                if (instance == null) {
                    GameObject obj = new GameObject();
                    instance = obj.AddComponent<DebugPrint>();
                    obj.name = "DebugPrint";
                    instance.printList = new List<PrintInfo>();
                    DontDestroyOnLoad(obj);
                }
                return instance;
            }
        }

        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

        /// <summary>prints a normal text to the debug window
        /// </summary>
        public void Print(string buffer) {
            PrintInfo data = new PrintInfo(buffer);
            printList.Add(data);
            scrollPosition.y = Mathf.Infinity;
        }

        //the gui crap
        void OnGUI() {
            windowRect = GUI.Window(windowId, windowRect, DebugWindow, "");
        }

        void DebugWindow(int windowId) {

            GUILayout.BeginArea(new Rect(xTextPrintOffset, 0, windowRect.width, windowRect.height));

            GUILayout.Space(2);
            GUILayout.Box("Debug Window");

            if (GUILayout.Button("Close")) {
                printList.Clear();
                GameObject.Destroy(this);
            }
            if (GUILayout.Button("Clear"))
                printList.Clear();

            scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(windowRect.width - 10), GUILayout.Height(windowRect.height - 80));

            for (int i = 0; i < printList.Count; i++) {
                var t = printList[i];
                if (t.boxBool)
                    GUILayout.Box(t.buffer);
                else
                    GUILayout.Label(t.buffer);
            }

            GUILayout.EndScrollView();

            GUILayout.EndArea();

            GUI.DragWindow();

        }
    }
}
