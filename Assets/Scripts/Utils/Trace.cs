﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Utils {
    public class Trace {
        public static bool IsDebug = true;
        public static bool ShowDebugWindow = false;
        public static bool UseMouseInput = true;
        public static bool UseKeyboardInput = false;
        public static bool UseMetadataCache = true;
        public static bool UseYoutubeCache = true;
        public static bool UseGameDriveCache = true;
        public static bool UseOculusInput = false;
        public static int CacheDuration = 60 * 24 * 1;

        private static ScrollRect Scroll = null;
        private static TMP_Text Content = null;

        public static void Log(string message) {
            if (!IsDebug)
                return;
            string text = string.Format("[WXD]: {0}", message);
            Debug.Log(text);
            if (Events.Instance && Events.Instance.m_DebugUI && ShowDebugWindow) {
                if (Scroll == null) {
                    Scroll = GameObject.Find("/DebugUI/MainCanvas/Info/Scroll View").GetComponent<ScrollRect>();
                }
                if (Content == null) {
                    Content = GameObject.Find("/DebugUI/MainCanvas/Info/Scroll View/Viewport/Content").GetComponent<TMP_Text>();
                }
                Content.text += string.Format("\n{0}", text);
                Scroll.verticalNormalizedPosition = 0;
            }
        }
        public static void LogError(string message) {
            Log(string.Format("[ERROR] {0}", message));
        }
    }
}
