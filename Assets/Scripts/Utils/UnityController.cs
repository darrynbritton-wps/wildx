using ControllerSelection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;

public class UnityController : MonoBehaviour
{
    //Unity GameObject Controller for all Functions in Trace
    [Header("Debug Controls")]
    public bool DebugMode = Assets.Scripts.Utils.Trace.IsDebug;
    public bool DebugWindow = Assets.Scripts.Utils.Trace.ShowDebugWindow;
    public bool MouseInput = Assets.Scripts.Utils.Trace.UseMouseInput;
    public bool KeyboardInput = Assets.Scripts.Utils.Trace.UseKeyboardInput;
    public bool MetadataCache = Assets.Scripts.Utils.Trace.UseMetadataCache;
    public bool YoutubeCache = Assets.Scripts.Utils.Trace.UseYoutubeCache;
    public bool GameDriveCache = Assets.Scripts.Utils.Trace.UseGameDriveCache;
    public bool OculusInput = Assets.Scripts.Utils.Trace.UseOculusInput;

    [Header("Input Style")]
    [Tooltip("To change input styles, toggle only the VRInput Checkbox")]
    public bool VRInput = false;
    [Tooltip("Do not change this toggle (this is for viewing only)")]
    public bool WebGL = true;

    private GameObject OVRCameraRig;

    void Start()
    {
        OVRCameraRig = GameObject.Find("OVRCameraRig");
    }

    void Update()
    {
        if (!VRInput)
        {
            WebGL = true;
            SystemUserSetup("WebGL");
        }
        else
        {
            VRInput = true;
            WebGL = false;
            SystemUserSetup("VR");
        }
    }

    void SystemUserSetup(string inputType)
    {
        switch (inputType)
        {
            case "VR":
                {
                    OVRCameraRig.GetComponent<OVRRawRaycaster>().enabled = true;
                    OVRCameraRig.GetComponent<OVRPointerVisualizer>().enabled = true;
                    OVRCameraRig.GetComponent<VREyeRaycaster>().enabled = false;
                }
                break;
            case "WebGL":
                {
                    OVRCameraRig.GetComponent<OVRRawRaycaster>().enabled = false;
                    OVRCameraRig.GetComponent<OVRPointerVisualizer>().enabled = false;
                    OVRCameraRig.GetComponent<VREyeRaycaster>().enabled = true;
                }
                break;
            default:
                break;
        }
    }
}
