﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WPM;

public class MarkerLoader : MonoBehaviour {

    [SerializeField] private WorldMapGlobe m_Globe;
    [SerializeField] private GameObject m_BaseMarker;

    void Awake() {
        if (!m_Globe || !m_BaseMarker)
            return;
        ////Debug.Log("Loading markers");
        //// TEMP
        //var markers = new List<Marker> {
        //    new Marker { Name = "South Africa", Latitude = -24f, Longitude = 25f, Flag = "za" }
        //};
        //foreach (var marker in markers) {
        //    //Debug.Log("Add marker: " + marker.Name);
        //    var markerObject = Instantiate(m_BaseMarker);
        //    m_Globe.calc.fromUnit = UNIT_TYPE.DecimalDegrees;
        //    m_Globe.calc.fromLatDec = marker.Latitude;
        //    m_Globe.calc.fromLonDec = marker.Longitude;
        //    if (m_Globe.calc.Convert()) {
        //        var position = m_Globe.calc.toSphereLocation;
        //        //Debug.Log("Position: " + position);
        //        markerObject.SetActive(true);
        //        m_Globe.AddMarker(markerObject, position, 0f);
        //    }
        //}
    }

    //public class Marker {
    //    public string Name { get; set; }
    //    public float Latitude { get; set; }
    //    public float Longitude { get; set; }
    //    public string Flag { get; set; }
    //}
}
