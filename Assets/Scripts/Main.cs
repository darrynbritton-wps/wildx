﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Assets.Scripts;
using Assets.Scripts.Utils;

using Google.Apis.YouTube.v3.Data;

using Newtonsoft.Json;

using UnityEditor;

using UnityEngine;
using UnityEngine.Networking;

using static AppLoader;

public static class Main {

    public static GameObject FindObject(this GameObject parent, string name) {
        Transform[] trs = parent.GetComponentsInChildren<Transform>(true);
        foreach (Transform t in trs) {
            //Trace.Log(t.name + " == " + name);
            if (t.name == name) {
                return t.gameObject;
            }
        }
        return null;
    }

    public static class Preferences {

        private static readonly string DateFormat = "dd-MM-yyyy HH:mm:ss";

        public static string CDNData {
            get { return GetString("CDNData"); }
            set { SetString("CDNData", value); }
        }

        public static DateTime CDNCacheDate {
            get { return GetDate("CDNCacheDate"); }
            set { SetDate("CDNCacheDate", value); }
        }

        public static string YouTubeData {
            get { return GetString("YouTubeData"); }
            set { SetString("YouTubeData", value); }
        }

        public static DateTime YouTubeCacheDate {
            get { return GetDate("YouTubeCacheDate"); }
            set { SetDate("YouTubeCacheDate", value); }
        }

        public static string GameDriveData {
            get { return GetString("GameDriveData"); }
            set { SetString("GameDriveData", value); }
        }

        public static DateTime GameDriveCacheDate {
            get { return GetDate("GameDriveCacheDate"); }
            set { SetDate("GameDriveCacheDate", value); }
        }

        public static bool TutorialShown {
            get { return GetString("TutorialShown", "False").Equals("True"); }
            set { SetString("TutorialShown", value.ToString()); }
        }

        public static string TutorialText {
            get { return GetString("TutorialText"); }
            set { SetString("TutorialText", value); }
        }

        public static string GetString(string key) {
            if (PlayerPrefs.HasKey(key)) {
                return PlayerPrefs.GetString(key);
            }
            return null;
        }

        public static string GetString(string key, string def) {
            if (PlayerPrefs.HasKey(key)) {
                return PlayerPrefs.GetString(key);
            } else {
                return def;
            }
        }

        public static void SetString(string key, string value) {
            PlayerPrefs.SetString(key, value);
            PlayerPrefs.Save();
        }

        public static DateTime GetDate(string key) {
            if (PlayerPrefs.HasKey(key)) {
                return DateTime.ParseExact(PlayerPrefs.GetString(key), DateFormat, null);
            }
            return DateTime.MinValue;
        }

        public static void SetDate(string key, DateTime value) {
            PlayerPrefs.SetString(key, value.ToString(DateFormat, null));
            PlayerPrefs.Save();
        }
    }

    public static class Web {
        public static async Task<string> Post(string url, string request) {
            return await Post(url, request, null);
        }
        public static async Task<string> Post(string url, string request, string token) {
            using (UnityWebRequest webRequest = UnityWebRequest.Post(url, "")) {
                using (UploadHandler handler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(request)) {
                    contentType = "application/json"
                }) {
                    if (!string.IsNullOrEmpty(token)) {
                        webRequest.SetRequestHeader("Authorization", "Bearer " + token);
                    }
                    webRequest.uploadHandler = handler;
                    webRequest.timeout = 15;
                    //Trace.Log(string.Format("Web Post: {0} (Token: {1}) | Data: {2}", url, token, request));
                    await webRequest.SendWebRequest();
                    while (!webRequest.isDone) {
                        await Task.Delay(50);
                    }
                    if (webRequest.isNetworkError || webRequest.isHttpError) {
                        //Trace.Log(string.Format("Web Request Failed: {0} ({1})", webRequest.error, webRequest.downloadHandler.text));
                        return null;
                    } else {
                        //Trace.Log(string.Format("Web Response: {0}", webRequest.downloadHandler.text));
                        return webRequest.downloadHandler.text;
                    }
                }
            }
        }
        public static async Task<string> Get(string url, string token) {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(url)) {
                if (!string.IsNullOrEmpty(token)) {
                    webRequest.SetRequestHeader("Authorization", "Bearer " + token);
                }
                webRequest.timeout = 15;
                //Trace.Log(string.Format("Web Get: {0} (Token: {1})", url, token));
                await webRequest.SendWebRequest();
                while (!webRequest.isDone) {
                    await Task.Delay(50);
                }
                if (webRequest.isNetworkError || webRequest.isHttpError) {
                    Trace.LogError(string.Format("Web Request Failed: {0} ({1})", webRequest.error, webRequest.downloadHandler.text));
                    return null;
                } else {
                    //Trace.Log(string.Format("Web Response: {0}", webRequest.downloadHandler.text));
                    return webRequest.downloadHandler.text;
                }
            }
        }
        public static async Task<T> Get<T>(string url, string token) {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(url)) {
                if (!string.IsNullOrEmpty(token)) {
                    webRequest.SetRequestHeader("Authorization", "Bearer " + token);
                }
                webRequest.timeout = 15;
                //Trace.Log(string.Format("Web Get: {0} (Token: {1})", url, token));
                await webRequest.SendWebRequest();
                while (!webRequest.isDone) {
                    await Task.Delay(50);
                }
                if (webRequest.isNetworkError || webRequest.isHttpError) {
                    Trace.LogError(string.Format("Web Request Failed: {0} ({1})", webRequest.error, webRequest.downloadHandler.text));
                    return default(T);
                } else {
                    //Trace.Log(string.Format("Web Response: {0}", webRequest.downloadHandler.text));
                    return JsonConvert.DeserializeObject<T>(webRequest.downloadHandler.text);
                }
            }
        }
        public static async Task<T> Post<T>(string url, string token) {
            using (UnityWebRequest webRequest = UnityWebRequest.Post(url, "")) {
                if (!string.IsNullOrEmpty(token)) {
                    webRequest.SetRequestHeader("Authorization", "Bearer " + token);
                }
                webRequest.timeout = 15;
                Trace.Log(string.Format("Web Request: {0} (Token: {1})", url, token));
                await webRequest.SendWebRequest();
                while (!webRequest.isDone) {
                    await Task.Delay(50);
                }
                if (webRequest.isNetworkError || webRequest.isHttpError) {
                    Trace.LogError(string.Format("Web Request Failed: {0} ({1})", webRequest.error, webRequest.downloadHandler.text));
                    return default(T);
                } else {
                    //Trace.Log(string.Format("Web Response: {0}", webRequest.downloadHandler.text));
                    return JsonConvert.DeserializeObject<T>(webRequest.downloadHandler.text);
                }
            }
        }
        public static async Task<T> Post<T>(string url, string request, string token) {
            using (UnityWebRequest webRequest = UnityWebRequest.Post(url, "")) {
                using (UploadHandler handler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(request)) {
                    contentType = "application/json"
                }) {
                    if (!string.IsNullOrEmpty(token)) {
                        webRequest.SetRequestHeader("Authorization", "Bearer " + token);
                    }
                    webRequest.uploadHandler = handler;
                    webRequest.timeout = 15;
                    Trace.Log(string.Format("Web Request: {0} (Token: {1}) | Data: {2}", url, token, request));
                    await webRequest.SendWebRequest();
                    while (!webRequest.isDone) {
                        await Task.Delay(50);
                    }
                    if (webRequest.isNetworkError || webRequest.isHttpError) {
                        Trace.Log(string.Format("Web Request Failed: {0} ({1})", webRequest.error, webRequest.downloadHandler.text));
                        return default(T);
                    } else {
                        Trace.Log(string.Format("Web Response: {0}", webRequest.downloadHandler.text));
                        return JsonConvert.DeserializeObject<T>(webRequest.downloadHandler.text);
                    }
                }
            }
        }
        public static async Task<T> Post<T>(string url, WWWForm form) {
            //List<IMultipartFormSection> data = new List<IMultipartFormSection> {
            //    new MultipartFormDataSection(form)
            //};
            using (UnityWebRequest webRequest = UnityWebRequest.Post(url, form)) {
                webRequest.timeout = 15;
                //Trace.Log(string.Format("Web Request: {0} | Data: {1}", url, form));
                await webRequest.SendWebRequest();
                while (!webRequest.isDone) {
                    await Task.Delay(50);
                }
                if (webRequest.isNetworkError || webRequest.isHttpError) {
                    //Trace.Log(string.Format("Web Request Failed: {0} ({1})", webRequest.error, webRequest.downloadHandler.text));
                    return default(T);
                } else {
                    //Trace.Log(string.Format("Web Response: {0}", webRequest.downloadHandler.text));
                    return JsonConvert.DeserializeObject<T>(webRequest.downloadHandler.text);
                }
            }
        }
    }

    public static class Graphics {

        private static Dictionary<string, Sprite> ThumbnailCache = new Dictionary<string, Sprite>();

        public static IEnumerator FromCache(VideoItemView view, bool download, Action<SpriteCallback> callback) {
            //lock (ThumbnailCache) {
            if (ThumbnailCache.ContainsKey(view.Video.ID)) {
                //Trace.Log("Thumbnails exists in cache: " + view.Video.ID);
                callback(new SpriteCallback { View = view, Sprite = ThumbnailCache[view.Video.ID] });
                yield break;
            }
            var path = string.Format("{0}/textures/{1}", Application.persistentDataPath, view.Video.ID);
            if (File.Exists(path)) {
                yield return MipmapTexture(File.ReadAllBytes(path), (texture) => {
                    var sprite = SpriteFromTexture2D(texture);
                    ToCache(view.Video.ID, sprite, false);
                    callback(new SpriteCallback { View = view, Sprite = sprite });
                });
            } else if (download) {
                if (view.Video.Thumbnail.StartsWith("asset-")) {
                    yield return AppLoader.Instance.BlobService.GetImageBlob((data) => {
                        if (data.IsError) {
                            Trace.LogError(data.ErrorMessage);
                        } else {
                            SpriteFromTexture2D(data.Data, view, (sprite) => {
                                ToCache(view.Video.ID, sprite.Sprite, true);
                                callback(sprite);
                            });
                        }
                    }, view.Video.Thumbnail);
                } else {
                    using (UnityWebRequest webRequest = UnityWebRequest.Get(view.Video.Thumbnail)) {
                        webRequest.timeout = 15;
                        yield return webRequest.SendWebRequest();
                        if (webRequest.isNetworkError || webRequest.isHttpError) {
                            Trace.LogError(string.Format("Web Request Failed: {0} ({1})", webRequest.error, webRequest.downloadHandler.text));
                        } else {
                            yield return MipmapTexture(webRequest.downloadHandler.data, (texture) => {
                                var sprite = SpriteFromTexture2D(texture);
                                ToCache(view.Video.ID, sprite, true);
                                callback(new SpriteCallback { View = view, Sprite = sprite });
                            });
                        }
                    }
                }
            }
            //}
        }

        public static IEnumerator Cache(VideoDataItem video) {
            var path = string.Format("{0}/textures/{1}", Application.persistentDataPath, video.ID);
            if (File.Exists(path)) {
                yield return MipmapTexture(File.ReadAllBytes(path), (texture) => {
                    var sprite = SpriteFromTexture2D(texture);
                    ToCache(video.ID, sprite, false);
                });
            } else {
                if (video.Thumbnail.StartsWith("asset-")) {
                    yield return AppLoader.Instance.BlobService.GetImageBlob((data) => {
                        if (data.IsError) {
                            Trace.LogError("Missing asset: " + video.Name + " (" + data.ErrorMessage + ")");
                        } else {
                            SpriteFromTexture2D(data.Data, null, (sprite) => {
                                ToCache(video.ID, sprite.Sprite, false);
                            });
                        }
                    }, video.Thumbnail);
                } else {
                    using (UnityWebRequest webRequest = UnityWebRequest.Get(video.Thumbnail)) {
                        webRequest.timeout = 15;
                        yield return webRequest.SendWebRequest();
                        if (webRequest.isNetworkError || webRequest.isHttpError) {
                            Trace.LogError(string.Format("Web Request Failed: {0} ({1})", webRequest.error, webRequest.downloadHandler.text));
                        } else {
                            yield return MipmapTexture(webRequest.downloadHandler.data, (texture) => {
                                var sprite = SpriteFromTexture2D(texture);
                                ToCache(video.ID, sprite, true);
                            });
                        }
                    }
                }
            }
        }

        public static void ToCache(string key, Sprite sprite, bool useDisk) {
            //lock (ThumbnailCache) {
            if (!ThumbnailCache.ContainsKey(key)) {
                ThumbnailCache.Add(key, sprite);
            }
            if (useDisk) {
                var path = string.Format("{0}/textures/{1}", Application.persistentDataPath, key);
                //Trace.Log(path);
                if (!Directory.Exists(Path.GetDirectoryName(path))) {
                    Directory.CreateDirectory(Path.GetDirectoryName(path));
                }
                //Trace.Log("Write cache: " + key);
                File.WriteAllBytes(path, sprite.texture.EncodeToJPG());
            }
            //}
        }

        public static void SpriteFromTexture2D(Texture2D texture, VideoItemView view, Action<SpriteCallback> callback) {
            Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f);
            callback.Invoke(new SpriteCallback { View = view, Sprite = sprite });
        }

        public static Sprite SpriteFromTexture2D(Texture2D texture) {
            return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f);
        }

        public static IEnumerator MipmapTexture(byte[] data, Action<Texture2D> callback) {
            Texture2D newTexture = new Texture2D(2, 2) {
                anisoLevel = 4,
                mipMapBias = -0.7f,
                filterMode = FilterMode.Trilinear,
                requestedMipmapLevel = 0
            };
            yield return newTexture.LoadImage(data);
            callback(newTexture);
        }

        public static Texture2D MipmapTexture(byte[] data) {
            Texture2D newTexture = new Texture2D(2, 2) {
                anisoLevel = 4,
                mipMapBias = -0.7f,
                filterMode = FilterMode.Trilinear,
                requestedMipmapLevel = 0
            };
            newTexture.LoadImage(data);
            return newTexture;
        }

        public class SpriteCallback {
            public VideoItemView View { get; set; }
            public Sprite Sprite { get; set; }
        }
    }

    public static class Strings {

        public static string GetCompactedNumber(ulong number) {
            if (number >= 1000) {
                return string.Format("{0:N0}k", number / 1000);
            } else if (number >= 1000000) {
                return string.Format("{0:N0}m", number / 1000000);
            } else {
                return number.ToString();
            }
        }

        public static IEnumerable<IEnumerable<T>> Split<T>(T[] array, int size) {
            for (var i = 0; i < (float)array.Length / size; i++) {
                yield return array.Skip(i * size).Take(size);
            }
        }

        public static Stream Stream(string s) {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
