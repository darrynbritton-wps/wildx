using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using VRStandardAssets.Utils;
using WPM;

namespace Assets.Scripts {

    [Serializable]
    public class InteractiveEvent : UnityEvent<GameObject> {
    }

    public class InteractiveUIItem : InteractiveItem {

        [SerializeField] public InteractiveEvent OnGazed;
        [SerializeField] public InteractiveEvent OnEnter;
        [SerializeField] public InteractiveEvent OnExit;
        [SerializeField] public InteractiveEvent OnHover;

        public new void Awake() {
            
        }

        protected override void HandleClick() {

        }

        protected override void HandleDoubleClick() {

        }

        protected override void HandleGazed(RaycastHit hit, GameObject item) {
            //Debug.Log("Gazed at: " + item.name);
            OnGazed?.Invoke(item);
        }

        protected override void HandleMove(RaycastHit hit, GameObject item) {
            OnHover?.Invoke(item);
        }

        protected override void HandleOut(GameObject item) {
            OnExit?.Invoke(item);
        }

        protected override void HandleOver(RaycastHit hit, GameObject item) {
            OnEnter?.Invoke(item);
        }

        public new void Update() {

        }
    }
}