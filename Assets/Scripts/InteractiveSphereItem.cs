using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using VRStandardAssets.Utils;
using WPM;

namespace Assets.Scripts {

    [Serializable]
    public class InteractiveSphereEvent : UnityEvent<RaycastHit, GameObject> {
    }

    public class InteractiveSphereItem : InteractiveItem {

        [SerializeField] public InteractiveSphereEvent OnGazed;
        [SerializeField] public InteractiveEvent OnEnter;
        [SerializeField] public InteractiveEvent OnExit;
        [SerializeField] public InteractiveSphereEvent OnHover;

        public new void Awake() {
            
        }

        protected override void HandleClick() {

        }

        protected override void HandleDoubleClick() {

        }

        protected override void HandleGazed(RaycastHit hit, GameObject item) {
            OnGazed?.Invoke(hit, item);
        }

        protected override void HandleMove(RaycastHit hit, GameObject item) {
            OnHover?.Invoke(hit, item);
        }

        protected override void HandleOut(GameObject item) {
            OnExit?.Invoke(item);
        }

        protected override void HandleOver(RaycastHit hit, GameObject item) {
            OnEnter?.Invoke(item);
        }

        public new void Update() {

        }
    }
}