using System;
using UnityEngine;

namespace VRStandardAssets.Utils {
    // This class should be added to any gameobject in the scene
    // that should react to input based on the user's gaze.
    // It contains events that can be subscribed to by classes that
    // need to know about input specifics to this gameobject.
    public class VRInteractiveItem : MonoBehaviour {
        public event Action<RaycastHit, GameObject> OnOver;
        public event Action<RaycastHit, GameObject> OnMove;
        public event Action<RaycastHit, GameObject> OnGazed;
        public event Action<GameObject> OnOut;
        public event Action OnClick;
        public event Action OnDoubleClick;
        public event Action OnUp;
        public event Action OnDown;
        [SerializeField] private bool m_IsGazeable = true;
        [SerializeField] private bool m_IsHoverable = true;

        protected RaycastHit m_LastHit;

        protected bool m_IsOver;
        public bool IsOver {
            get { return m_IsOver; }
        }

        public bool IsGazeable {
            get { return m_IsGazeable; }
            set { m_IsGazeable = value; }
        }

        public bool IsHoverable {
            get { return m_IsHoverable; }
            set { m_IsHoverable = value; }
        }

        public RaycastHit LastHit {
            get { return m_LastHit; }
        }

        public void Over(RaycastHit point, GameObject item) {
            m_IsOver = true;
            OnOver?.Invoke(point, item);
        }

        public void Move(RaycastHit point, GameObject item) {
            OnMove?.Invoke(point, item);
        }

        public void Gazed(RaycastHit point, GameObject item) {
            OnGazed?.Invoke(point, item);
        }

        public void Out(GameObject item) {
            m_IsOver = false;
            OnOut?.Invoke(item);
        }

        public void Click() {
            OnClick?.Invoke();
        }

        public void DoubleClick() {
            OnDoubleClick?.Invoke();
        }

        public void Up() {
            OnUp?.Invoke();
        }

        public void Down() {
            OnDown?.Invoke();
        }
    }
}