using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using VRStandardAssets.Utils;
using WPM;
using static AppLoader;

namespace Assets.Scripts {
    public class InteractiveMapItem : InteractiveItem {

        [SerializeField] private WorldMapGlobe m_Globe;
        [SerializeField] public TMP_Text m_Region;
        private Vector3 m_Position;
        private GameObject m_MarkerInstance;

        public new void Awake() {
            
        }

        protected override void HandleClick() {
            
        }

        protected override void HandleDoubleClick() {
            
        }

        protected override void HandleGazed(RaycastHit hit, GameObject item) {
            if (m_Globe) {
                if (Instance) {
                    Instance.LoadVideos(item.name, true);
                }
            }
        }

        protected override void HandleMove(RaycastHit hit, GameObject item) {
            
        }

        protected override void HandleOut(GameObject item) {
            
        }

        protected override void HandleOver(RaycastHit hit, GameObject item) {
            
        }

        public new void Update() {

        }
    }
}