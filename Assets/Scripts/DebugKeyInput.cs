﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Utils;
using UnityEngine;
using VRStandardAssets.Utils;

namespace Assets.Scripts {
    public class DebugKeyInput : MonoBehaviour {

        [SerializeField] private InteractiveUIItem AKey;
        [SerializeField] private InteractiveUIItem DKey;

        public void Awake() {
            Trace.Log("DebugKeyInput Active");
        }

        public void LateUpdate() {
            if (!Trace.IsDebug)
                return;
            if (Input.GetKey(KeyCode.A) && AKey) {
                Trace.Log("AKey: " + Input.GetKey(KeyCode.A));
                AKey.OnHover.Invoke(AKey.gameObject);
            }
            if (Input.GetKey(KeyCode.D) && DKey) {
                Trace.Log("DKey: " + Input.GetKey(KeyCode.D));
                DKey.OnHover.Invoke(DKey.gameObject);
            }
        }

    }
}
