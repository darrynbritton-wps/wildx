﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleAllItem : MonoBehaviour {
    [SerializeField] public Toggle Toggle;
    [SerializeField] public GridLayoutGroup Layout;
}