﻿/************************************************************************************

Copyright   :   Copyright 2017-Present Oculus VR, LLC. All Rights reserved.

Licensed under the Oculus VR Rift SDK License Version 3.2 (the "License");
you may not use the Oculus VR Rift SDK except in compliance with the License,
which is provided at the time of installation or download, or which
otherwise accompanies this software in either electronic or hard copy form.

You may obtain a copy of the License at

http://www.oculusvr.com/licenses/LICENSE-3.2

Unless required by applicable law or agreed to in writing, the Oculus VR SDK
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

************************************************************************************/

using Assets.Scripts;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using VRStandardAssets.Utils;

namespace ControllerSelection {
    public class OVRRawRaycaster : MonoBehaviour {
        [System.Serializable]
        public class HoverCallback : UnityEvent<Transform> { }
        [System.Serializable]
        public class SelectionCallback : UnityEvent<Transform> { }

        [Header("(Optional) Tracking space")]
        [Tooltip("Tracking space of the OVRCameraRig.\nIf tracking space is not set, the scene will be searched.\nThis search is expensive.")]
        public Transform trackingSpace = null;

        [SerializeField] private Reticle m_Reticle;                     // The reticle, if applicable.

        [Header("Selection")]
        [Tooltip("Primary selection button")]
        public OVRInput.RawButton primaryButton = OVRInput.RawButton.RIndexTrigger;
        [Tooltip("Secondary selection button")]
        public OVRInput.RawButton secondaryButton = OVRInput.RawButton.LIndexTrigger;
        [Tooltip("Layers to exclude from raycast")]
        public LayerMask excludeLayers;
        [Tooltip("Maximum raycast distance")]
        public float raycastDistance = 500;

        [Header("Hover Callbacks")]
        public OVRRawRaycaster.HoverCallback onHoverEnter;
        public OVRRawRaycaster.HoverCallback onHoverExit;
        public OVRRawRaycaster.HoverCallback onHover;

        [Header("Selection Callbacks")]
        public OVRRawRaycaster.SelectionCallback onPrimarySelect;
        public OVRRawRaycaster.SelectionCallback onSecondarySelect;

        //protected Ray pointer;
        protected Transform lastHit = null;
        protected Transform triggerDown = null;
        protected Transform padDown = null;

        private VRInteractiveItem m_CurrentInteractible;                //The current interactive item
        private VRInteractiveItem m_LastInteractible;                   //The last interactive item

        [HideInInspector]
        public OVRInput.Controller activeController = OVRInput.Controller.None;

        void Awake() {
            if (trackingSpace == null) {
                Debug.LogWarning("OVRRawRaycaster did not have a tracking space set. Looking for one");
                trackingSpace = OVRInputHelpers.FindTrackingSpace();
            }
        }

        void OnEnable() {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        void OnDisable() {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
            if (trackingSpace == null) {
                Debug.LogWarning("OVRRawRaycaster did not have a tracking space set. Looking for one");
                trackingSpace = OVRInputHelpers.FindTrackingSpace();
            }
        }

        private void DeactiveLastInteractible() {
            if (m_LastInteractible == null)
                return;

            VRInputModule.PointerExit(m_LastInteractible.gameObject);
            m_LastInteractible.Out(m_LastInteractible.gameObject);
            m_LastInteractible = null;
        }

        void Update() {
            activeController = OVRInputHelpers.GetControllerForButton(OVRInput.Button.PrimaryIndexTrigger, activeController);
            //activeController = OVRInput.GetActiveController();
            Ray pointer = OVRInputHelpers.GetSelectionRay(activeController, trackingSpace);

            RaycastHit hit; // Was anything hit?
            if (Physics.Raycast(pointer, out hit, raycastDistance, ~excludeLayers)) {

                //Debug.Log(hit.collider.gameObject.name);
                VRInteractiveItem interactible = hit.collider.GetComponent<VRInteractiveItem>(); //attempt to get the VRInteractiveItem on the hit object
                m_CurrentInteractible = interactible;

                if (m_Reticle) {
                    m_Reticle.SetPosition(hit);
                    //Debug.Log("Hit: " + hit.point);
                }

                // If we hit an interactive item and it's not the same as the last interactive item, then call Over
                if (interactible && interactible != m_LastInteractible) {
                    interactible.Over(hit, interactible.gameObject);
                    VRInputModule.PointerEnter(interactible.gameObject);
                } else {
                    if (interactible) {
                        interactible.Move(hit, interactible.gameObject);
                        if (interactible.IsGazeable) {
                            
                        } else if (interactible.IsHoverable) {

                        }
                    }
                }

                // Deactive the last interactive item 
                if (interactible != m_LastInteractible) {
                    DeactiveLastInteractible();
                }

                m_LastInteractible = interactible;

                if (lastHit != null && lastHit != hit.transform) {
                    if (onHoverExit != null) {
                        onHoverExit.Invoke(lastHit);
                    }
                    lastHit = null;
                }

                if (lastHit == null) {
                    if (onHoverEnter != null) {
                        onHoverEnter.Invoke(hit.transform);
                    }
                }

                if (onHover != null) {
                    onHover.Invoke(hit.transform);
                }

                lastHit = hit.transform;

                // Handle selection callbacks. An object is selected if the button selecting it was
                // pressed AND released while hovering over the object.
                if (activeController != OVRInput.Controller.None) {
                    if (OVRInput.GetDown(secondaryButton, activeController)) {
                        padDown = lastHit;
                    }
                    else if (OVRInput.GetUp(secondaryButton, activeController)) {
                        if (padDown != null && padDown == lastHit) {
                            if (onSecondarySelect != null) {
                                onSecondarySelect.Invoke(padDown);
                            }
                        }
                    }
                    if (!OVRInput.Get(secondaryButton, activeController)) {
                        padDown = null;
                    }

                    if (OVRInput.GetDown(primaryButton, activeController)) {
                        triggerDown = lastHit;
                    }
                    else if (OVRInput.GetUp(primaryButton, activeController)) {
                        if (triggerDown != null && triggerDown == lastHit) {
                            if (onPrimarySelect != null) {
                                onPrimarySelect.Invoke(triggerDown);
                            }
                            if (interactible != null) {
                                VRInputModule.PointerSubmit(interactible.gameObject);
                                interactible.Gazed(hit, interactible.gameObject);
                            }
                        }
                    }
                    if (!OVRInput.Get(primaryButton, activeController)) {
                        triggerDown = null;
                    }
                }
#if UNITY_ANDROID && !UNITY_EDITOR
            // Gaze pointer fallback
            else {
                if (Input.GetMouseButtonDown(0) ) {
                    triggerDown = lastHit;
                }
                else if (Input.GetMouseButtonUp(0) ) {
                    if (triggerDown != null && triggerDown == lastHit) {
                        if (onPrimarySelect != null) {
                            onPrimarySelect.Invoke(triggerDown);
                        }
                    }
                }
                if (!Input.GetMouseButton(0)) {
                    triggerDown = null;
                }
            }
#endif
            }
            // Nothing was hit, handle exit callback
            else if (lastHit != null) {
                if (onHoverExit != null) {
                    onHoverExit.Invoke(lastHit);
                }
                lastHit = null;
                if (m_Reticle) {
                    m_Reticle.SetPosition();
                }
            }
        }
    }
}