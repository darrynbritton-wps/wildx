﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assets.Scripts;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using WildX.Web.Models;

namespace WildX.Web.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class ProxyController : ControllerBase {

        //private static string AZUrl = "https://management.azure.com/subscriptions/e877a478-df73-491c-9c9c-947a9f80fcc3/resourceGroups/wpsVRApp/providers/Microsoft.Media/mediaServices/wpsvrmedia/{0}?api-version=2018-07-01";
        public static string ProxyUrl = "https://wildx.azurewebsites.net";
        private IWebHostEnvironment _environment;

        public ProxyController(IWebHostEnvironment environment) {
            _environment = environment;
        }

        [Route("token")]
        [HttpGet]
        public string Token() {
            try {
                RestRequest request = new RestRequest("https://login.microsoftonline.com/97d7aef4-3768-471f-bfa2-fac54183d5a5/oauth2/token", Method.POST);
                request.AddParameter("grant_type", "client_credentials");
                request.AddParameter("client_id", "80cc3402-b07d-492f-a2f4-e91b1a90b9fb");
                request.AddParameter("client_secret", "ZdJLl_-JSIQ38rtf561P6VYMB_CY-M5F08");
                request.AddParameter("resource", "https://management.core.windows.net/");
                RestClient client = new RestClient();
                var response = client.Execute(request);
                if (response != null && response.IsSuccessful) {
                    dynamic data = JsonConvert.DeserializeObject(response.Content);
                    return data.access_token;
                } else if (response != null) {
                    return "Error " + response.StatusCode.ToString() + ": " + response.ErrorMessage + " (" + response.Content + ")";
                }
            } catch (Exception e) {
                return "Error: " + e.Message;
            }
            return "Error: fetch token failed";
        }

        [HttpPost]
        [Route("youtube")]
        public List<VideoDataItem> Youtube(YoutubeModel model) {
            var result = new List<VideoDataItem>();
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            System.Net.ServicePointManager.ServerCertificateValidationCallback = (message, cert, chain, sslPolicyErrors) => true;
            var youtubeService = new YouTubeService(new BaseClientService.Initializer() {
                ApiKey = "AIzaSyDbgPJ_Cdg61Y5T5j5mffAT4AZkdXpHYQU",
                ApplicationName = this.GetType().ToString()
            });
            List<string[]> idList = new List<string[]>();
            var parts = model.IDs.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            var splitParts = Utils.Split(parts, 30);
            foreach (var part in splitParts) {
                var videoListRequest = youtubeService.Videos.List("snippet,contentDetails,statistics");
                videoListRequest.Id = string.Join(",", part);
                var videoListResponse = videoListRequest.Execute();
                foreach (var videoListItem in videoListResponse.Items) {
                    var videoItem = new VideoDataItem {
                        ID = videoListItem.Id,
                        Name = videoListItem.Snippet.Title,
                        //Thumbnail = videoListItem.Snippet.Thumbnails?.Maxres?.Url
                        Thumbnail = GetThumbnailUrl(videoListItem.Snippet.Thumbnails?.Maxres?.Url)
                    };
                    if (videoListItem.Snippet.PublishedAt.HasValue) {
                        videoItem.DateCreated = videoListItem.Snippet.PublishedAt.Value;
                    }
                    videoItem.TotalLikes = videoListItem.Statistics.LikeCount.HasValue ? videoListItem.Statistics.LikeCount.Value : 0;
                    videoItem.TotalDislikes = videoListItem.Statistics.DislikeCount.HasValue ? videoListItem.Statistics.DislikeCount.Value : 0;
                    videoItem.TotalViews = videoListItem.Statistics.ViewCount.HasValue ? videoListItem.Statistics.ViewCount.Value : 0;
                    result.Add(videoItem);
                }
            }
            return result;
        }

        private string GetThumbnailUrl(string url) {
            if (string.IsNullOrEmpty(url))
                return "";
            Uri uri = new Uri(url);
            string name = uri.Segments[uri.Segments.Length - 2].Replace("/", "");
            return string.Format("{0}/proxy/image?id={1}", ProxyUrl, name);
        }

        [HttpGet]
        [Route("image")]
        public FileContentResult Image(string id) {
            if (string.IsNullOrEmpty(id))
                return null;
            Uri uri = new Uri(string.Format("https://i.ytimg.com/vi/{0}/maxresdefault.jpg", id));
            string name = string.Format("{0}.jpg", uri.Segments[uri.Segments.Length - 2]).Replace("/", "");
            string path = System.IO.Path.Combine(_environment.WebRootPath, "Cache", "Thumbnails");
            string fileName = System.IO.Path.Combine(path, name);
            if (!System.IO.Directory.Exists(path)) {
                System.IO.Directory.CreateDirectory(path);
            }
            if (System.IO.File.Exists(fileName)) {
                return File(System.IO.File.ReadAllBytes(fileName), "image/jpeg");
            }
            using (System.Net.WebClient client = new System.Net.WebClient()) {
                client.DownloadFile(uri.ToString(), fileName);
                if (System.IO.File.Exists(fileName)) {
                    return File(System.IO.File.ReadAllBytes(fileName), "image/jpeg");
                }
            }
            return null;
        }

        [HttpPost]
        [Route("image")]
        public FileContentResult Image(ImageModel model) {
            Uri uri = new Uri(model.URL);
            string name = string.Format("{0}.jpg", uri.Segments[uri.Segments.Length - 2]).Replace("/", "");
            string path = System.IO.Path.Combine(_environment.WebRootPath, "Cache", "Thumbnails");
            string fileName = System.IO.Path.Combine(path, name);
            if (!System.IO.Directory.Exists(path)) {
                System.IO.Directory.CreateDirectory(path);
            }
            if (System.IO.File.Exists(fileName)) {
                return File(System.IO.File.ReadAllBytes(fileName), "image/jpeg");
            }
            using (System.Net.WebClient client = new System.Net.WebClient()) {
                client.DownloadFile(model.URL, fileName);
                if (System.IO.File.Exists(fileName)) {
                    return File(System.IO.File.ReadAllBytes(fileName), "image/jpeg");
                }
            }
            return null;
        }
    }
}
