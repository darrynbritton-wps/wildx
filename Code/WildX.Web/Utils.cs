﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WildX.Web {
    public class Utils {
        public static IEnumerable<IEnumerable<T>> Split<T>(T[] array, int size) {
            for (var i = 0; i < (float)array.Length / size; i++) {
                yield return array.Skip(i * size).Take(size);
            }
        }
    }
}
