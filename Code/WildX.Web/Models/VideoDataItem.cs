﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts {
    public class VideoDataItem {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Overview { get; set; }
        //public string Format { get; set; }
        public DateTime DateCreated { get; set; }
        public string Thumbnail { get; set; }
        public string Url { get; set; }
        public List<VideoAttribute> Metadata { get; set; }
        public ulong TotalViews { get; set; }
        public ulong TotalLikes { get; set; }
        public ulong TotalDislikes { get; set; }
        public float Rating { get; set; }
        public bool IsFavorite { get; set; }
        public bool IsDownloaded { get; set; }
        //public VideoStats Stats { get; set; }
        //public List<VideoCustomAttribute> CustomAttributes { get; set; }
        //public List<VideoContent> Content { get; set; }
    }

    public class VideoAttribute {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
