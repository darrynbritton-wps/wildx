﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace WildX.API.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class ProxyController : ControllerBase {
        
        //private static string AZUrl = "https://management.azure.com/subscriptions/e877a478-df73-491c-9c9c-947a9f80fcc3/resourceGroups/wpsVRApp/providers/Microsoft.Media/mediaServices/wpsvrmedia/{0}?api-version=2018-07-01";

        [Route("token")]
        [HttpGet]
        public string Token() {
            try {
                RestRequest request = new RestRequest("https://login.microsoftonline.com/97d7aef4-3768-471f-bfa2-fac54183d5a5/oauth2/token", Method.POST);
                request.AddParameter("grant_type", "client_credentials");
                request.AddParameter("client_id", "80cc3402-b07d-492f-a2f4-e91b1a90b9fb");
                request.AddParameter("client_secret", "TNAq8h0m6.n7M.J~v6xgm-F8N7M-AEeu6J");
                request.AddParameter("resource", "https://management.core.windows.net/");
                RestClient client = new RestClient();
                var response = client.Execute(request);
                if (response != null && response.IsSuccessful) {
                    dynamic data = JsonConvert.DeserializeObject(response.Content);
                    return data.access_token;
                }
            } catch (Exception e) {
                return "Error: " + e.Message;
            }
            return "Error: fetch token failed";
        }
    }
}
